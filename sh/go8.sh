# for article testing -- excitation across wavefield, speed of sound

ssr-wfs -c ../ssr/conf/test8.conf --ip-server=6000 -n WFS  &
sleep 2
# ssr-wfs -c ../ssr/conf/test7.conf --ip-server=6001 -n WFS2  &
# sleep 2
ssr-binaural -c ../ssr/conf/test8b.conf --ip-server=5000 -n Binaural  &
# sleep 2
snd ../wav/test.wav &
sleep 2

jmess -D
jmess -c ../xml/go8.xml

