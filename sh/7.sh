#!/bin/bash
ssr-wfs -c ../ssr/conf/7wfs.conf --ip-server=6000 -n WFS  &

if [ "$1" != 'WFS' ]; then
  echo "BINAURAL"
  ssr-binaural -c ../ssr/conf/7binwfs.conf --ip-server=5000 -n Binaural   &
else
  echo "WFS"
fi
sleep 2

snd ../wav/sharpForceHammer.wav &

../qt/waveguideMeshSSRclient/waveguideMeshSSRclient &
sleep 3

# chuck --srate:48000 --channels:4 strikePosSaw.ck &

jmess -D
if [ "$1" != 'WFS' ]; then
  echo "BINAURAL"
  jmess -c ../xml/7binwfs.xml
else
  echo "WFS"
#  jmess -c 7wfs.xml
fi

