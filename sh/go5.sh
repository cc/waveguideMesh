# for article testing -- excitation across wavefield, speed of sound

ssr-wfs -c ../ssr/conf/test5.conf --ip-server=6000 -n WFS  &
sleep 2
ssr-binaural -c ../ssr/conf/test3.conf --ip-server=5000 -n Binaural  &
sleep 2
snd ../wav/test.wav &

sleep 2

jmess -D
jmess -c ../xml/go5.xml

