<?xml version="1.0"?>
<asdf>
  <scene_setup>
    <volume>0</volume>
    <reference>
      <position x="0" y="0"/>
      <orientation azimuth="90"/>
    </reference>
    <source name="live input 1" model="point" mute="false" volume="0">
      <port>1</port>
      <position x="-0.2232" y="1.01"/>
      <orientation azimuth="-77.5385"/>
    </source>
    <source name="live input 2" model="point" mute="false" volume="0">
      <port>2</port>
      <position x="-0.186" y="1.01"/>
      <orientation azimuth="-79.5654"/>
    </source>
    <source name="live input 3" model="point" mute="false" volume="0">
      <port>3</port>
      <position x="-0.1488" y="1.01"/>
      <orientation azimuth="-81.6191"/>
    </source>
    <source name="live input 4" model="point" mute="false" volume="0">
      <port>4</port>
      <position x="-0.1116" y="1.01"/>
      <orientation azimuth="-83.6947"/>
    </source>
    <source name="live input 5" model="point" mute="false" volume="0">
      <port>5</port>
      <position x="-0.0744" y="1.01"/>
      <orientation azimuth="-85.787"/>
    </source>
    <source name="live input 6" model="point" mute="false" volume="0">
      <port>6</port>
      <position x="-0.0372" y="1.01"/>
      <orientation azimuth="-87.8907"/>
    </source>
    <source name="live input 7" model="point" mute="false" volume="0">
      <port>7</port>
      <position x="0" y="1.01"/>
      <orientation azimuth="-90"/>
    </source>
    <source name="live input 8" model="point" mute="false" volume="0">
      <port>8</port>
      <position x="0.0372" y="1.01"/>
      <orientation azimuth="-92.1093"/>
    </source>
    <source name="live input 9" model="point" mute="false" volume="0">
      <port>9</port>
      <position x="0.0744" y="1.01"/>
      <orientation azimuth="-94.213"/>
    </source>
    <source name="live input 10" model="point" mute="false" volume="0">
      <port>10</port>
      <position x="0.1116" y="1.01"/>
      <orientation azimuth="-96.3053"/>
    </source>
    <source name="live input 11" model="point" mute="false" volume="0">
      <port>11</port>
      <position x="0.1488" y="1.01"/>
      <orientation azimuth="-98.3809"/>
    </source>
    <source name="live input 12" model="point" mute="false" volume="0">
      <port>12</port>
      <position x="0.186" y="1.01"/>
      <orientation azimuth="-100.435"/>
    </source>
  </scene_setup>
</asdf>
