# for article testing -- excitation across wavefield, speed of sound

ssr-wfs -c ../ssr/conf/test4.conf --ip-server=6000 -n WFS  &
ssr-binaural -c ../ssr/conf/test3.conf --ip-server=5000 -n Binaural  &

sleep 2

snd ../wav/test.wav &

sleep 1

jmess -D
jmess -c ../xml/go4.xml

