Fig 9a is pureOrder1, no contact sound 12ch
Fig 10a normalized (supresses plate in order to see force hammer)
#define IN_GAIN 1.0
#define OUT_GAIN 0.01



  inputs_[0] = gain_ * input;
  lastFrame_[0] =  1 * inputs_[2];
  lastFrame_[0] -= 0;
  inputs_[2] = inputs_[1];
  inputs_[1] = inputs_[0];
  outputs_[2] = outputs_[1];
  outputs_[1] = lastFrame_[0];

///////////

// no stretch
//#define stretchOff 0.0f
//#define stretchSclNeg 0.0f
//#define stretchSclPos 0.00f
// stretch
#define stretchOff 0.75f
#define stretchSclNeg 0.0f
#define stretchSclPos 0.00f
// pie pan
//#define stretchOff 0.75f
//#define stretchSclNeg 0.2f
//#define stretchSclPos 0.2f
// JRP
//#define stretchOff 0.75f
//#define stretchSclNeg -0.5f
//#define stretchSclPos 0.003f
#define ORDER 4
