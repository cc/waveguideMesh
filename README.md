examples for [ICSV 2019](https://www.icsv26.org/) for the paper:

[Extensions to the 2D Waveguide Mesh for Modeling Thin Plate Vibrations](https://ccrma.stanford.edu/~cc/pub/pdf/extensionsTo2DMeshChafe8pages.pdf)

uses

* fork of CC's git-mesh C++ project 

* [SoundScape Renderer](http://spatialaudio.net/ssr/) built from [source](https://bitbucket.org/spatialaudio/ssr/downloads/ssr-0.5.0.tar.gz) on Fedora 29

* snd for file playback

***
1. fork **git-mesh** (historical, this is the start)
  1. (requires account access, see cc)
  2. `git clone cmn9.stanford.edu:/user/c/cc/git-mesh.git`
  3. `rm -rf .git`
  4. copy the qt/tmeshRt subdirectory into this project
  5. `cd qt; ./clone.sh tmeshRt waveguideMesh`
2. install **stk** (and explicitly link the library version)
  1. `./configure --with-jack`, `make`, `make install` **stk** from [src](https://ccrma.stanford.edu/software/stk/download.html)
  2. `sudo ln /usr/local/lib/libstk-4.6.1.so /lib64/`
3. build  **waveguideMesh** or  **waveguideMeshSSRclient**
  0. depends on stk (see above)
  1. open waveguideMesh.pro in QtCreator
  2. configure
  3. build
  4. beware that the creator project may default to debug mode which is very slow compared to release
  5. can also be compiled `qmake-qt5`, `make` (defaults to release)
4. install **ssr** on Fedora 29 (noting dependencies)
  1. `dnf install libsndfile-devel fftw-devel libxm2-devel asio-devel libsamplerate-devel`
  2. on Fedora 29 needs `ln /lib64/qt5/bin/moc /usr/bin/`
  3. `./configure`, `make`, `make install`  **ecasound** from [src](http://ecasound.seul.org/download/ecasound-2.9.1.tar.gz)
  4. `./configure`, `make`, `make install` **ssr** from [src](https://bitbucket.org/spatialaudio/ssr/downloads/ssr-0.5.0.tar.gz) [(read more)](https://ssr.readthedocs.io/en/0.5.0/operation.html#installing)
5. install **snd** (noting dependencies)
  1. `dnf install motif-devel libXpm-devel`
  2. `./configure --with-motif --with-jack`, `make`, `make install` **snd** from src included in this project under snd-19.3-59outputChans (to have multi-channel connection to jack audio)
6. install **chuck**
  1. `cd src; make --linux-jack`, `make install` **chuck** from [src](http://chuck.stanford.edu/release/)
7. install **gnuplot**
  1. `dnf install gnuplot`
8. install **jmess**
  1. `cd jmess/src; ./build , make install` **jmess** from [src](https://github.com/jcacerec/jmess-jack)


***
####  mono mesh output / binaural
play 1-ch test.wav from waveguideMesh through ssr-binaural

1. #define N_OUTCHANS 1 in creator
2. hit run to write test.wav (sharpForceHammer plate strike)
3. `cd sh; ./go.sh`
4. patch snd to binaural in qjackctl
5. quit with `./stop.sh`

####  mono impulse / wfs / plot
play sharpForceHammer impulse through ssr-wfs and plot wavefront

1. `./go2.sh`
2. patch snd to wfs and wfs to recorder
3. hit ssr play, hit snd play, select ssr quit
4. quit with `./stop.sh`
5. open test.wav in audacity, select impulse clip, workaround audacity bug: (ch1) make stereo track, then split stereo to mono, ch1&2 are now mono like the rest
6. export multi, edit ck/convert2.ck so nChans and name prefix from export match the set of mono files it uses
7. `cd ck; chuck -s --caution-to-the-wind convert2.ck`
8. plot can be saved manually

####  mono impulse / wfs / binaural
play sharpForceHammer impulse through ssr-wfs and into ssr-binaural

1. `./go3.sh`
2. patch snd to wfs and wfs to binaural
3. record with audacity
4. quit with `./stop.sh`

####  multi mesh output / wfs / binaural
generate 12-ch test.wav, play through ssr-wfs and into ssr-binaural

1. #define N_OUTCHANS 12 in creator and run it
2. `./go4.sh`
2. patch snd to wfs and wfs to binaural
3. quit with `./stop.sh`

***
##### new branch

+ `git checkout -b newbranch`
+ `git push origin newbranch`
+ `git branch`

##### on another remote

+ `git fetch origin newbranch:newbranch`
+ `git branch`

##### merge and delete branch

+ `git push origin newbranch`
+ `git checkout master`
+ `git reflog`(displays state)
+ `git checkout newbranch`
+ `git checkout -b candidate-newbranch`
+ resolve any discrepancies using the candidate
+ `git checkout master`
+ `git merge candidate-newbranch master`
+ `git push origin master`
+ `git branch -d candidate-newbranch`
+ `git branch -d newbranch`