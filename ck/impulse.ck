// chuck -s impulse.ck

WvOut w;
w.wavFilename("../wav/impulse.wav");
Impulse s;

w => blackhole;
1::second => now;
s => w;
s.next(1.0);
1::second => now;

