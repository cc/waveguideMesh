unset key
unset xtics
unset ytics
unset border
set style data lines
set style line 10 linewidth 1.0 linecolor rgb "red" 
set xrange[0:0.0029]
set yrange[0:16]
plot for [i=1:15] '/tmp/plotWFS.dat' using ($0/48000):(column(i)+(15-i) + 1) ls 10

