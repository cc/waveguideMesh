// chuck -s 31live_inputs_from_31MeshOutputs.ck
// plate =  31 by 8 spatial sample 0.0211667

31 => int nChans;
FileIO fout;
fout.open("../ssr/conf/scenes/31live_inputs_from_31MeshOutputs.asd",FileIO.WRITE);

"<?xml version=\"1.0\" encoding=\"utf-8\"?>

<asdf
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
  xsi:noNamespaceSchemaLocation=\"asdf.xsd\"
  version=\"0.1\">

  <header>
    <name>"+nChans+" live inputs</name>
    <description>
      This scene creates "+nChans+" sound sources and connects them
      to the first "+nChans+" inputs of your sound card (if available).
    </description>
  </header>

  <scene_setup>
" => string tmp;

0.0211667 => float xinc;
(nChans/2) * -xinc => float xleft;
1.01 => float y;

for (1 => int i; i <= nChans; i++) {
xleft + ((i-1)*xinc) => float x;
"
    <source name=\"live input "+i+"\" model=\"point\">
      <port>"+i+"</port>
      <position x=\""+x+"\" y=\""+y+"\"/>
    </source>
" +=> tmp;
}

"
  </scene_setup>
</asdf>
" +=> tmp;
fout <= tmp;
fout.close();

