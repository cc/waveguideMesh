// chuck -s --srate:48000 strikeTrain.ck
"../wav/" => string dir;

Impulse imp => LPF lpf1 => PoleZero pz => dac.chan(0);
lpf1.freq(3000.0);
imp => HPF hpf2 => pz;
hpf2.freq(12000.0);
pz.blockZero(0.95);

SndBuf snd;
dac => WvOut w => blackhole;
// snd => WvOut w => blackhole;
w.wavFilename(dir + "strikeTrain.wav");
0.5::second => now;
snd.read(dir + "sharpForceHammerClip.wav");
true => int go;

fun void clap()
{
  while (go)
  {
lpf1.freq(3000.0 + 2950.0*Std.rand2f(-1.0,1.0));
hpf2.freq(12000.0 + 1100.0*Std.rand2f(-1.0,1.0));
    imp.next(0.0 + 1.5*Std.rand2f(-1.0,1.0));
snd.pos(0);
    .1::second => now;
  }
}
spork ~clap();

now + 60::second => time quit;
while (now < quit)
{
  1::samp => now;
}
false => go;
5::second => now;
