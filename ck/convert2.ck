// chuck -s --caution-to-the-wind convert2.ck

// one file for raster / waterfall

15 => int nChans;
// 12 => int nChans;
SndBuf s[nChans];
Delay d[nChans];
FileIO fout;

for (0 => int i; i< nChans; i++) {
  i+1 => int a; // audacity numbering 1-based
  "0" => string tens;
  if (a>9) "" => tens;
  s[i].read("/tmp/test-"+tens+a+".wav");
  s[i] => d[i] => blackhole;
  d[i].delay(20::samp);
  fout.open("/tmp/plotWFS.dat", FileIO.WRITE);
}

false => int trig;
48000 => int quit;
for (0 => int i; i< 48000; i++) {
  for (0 => int j; j< nChans; j++) {
    s[j].last() => float trigVal;
//  if (!trig && (trigVal > 0.1) && (j == 3)) { true => trig; <<<i,j>>>; i+180 => quit;}
    if (!trig && (trigVal > 0.1) )            { true => trig; <<<i,j>>>; i+180 => quit;}
    d[j].last() => float val;
    if (trig) fout <= val + "\t";
  }
  if (trig) fout <= "\n";
  1::samp => now;
  if (i>quit) break;
}

fout.close();

Std.system ("gnuplot -e \"\" plot.txt -persist");
//Std.system ("gnuplot -e \"\" plot12ch.txt -persist");

