// chuck -s dataReader.ck writeInflatedGPDlongSaw.ck
// gdpCurSaw.wav = a table saw with blade speed from gdp, bite from cur
// gdp = GDP
// cur = Civilian Unemployment Rate
// /home/cc/Desktop/barnet/ness/50.8-withOffset  1-Mar-2015
// 18555 strikes

"../dat/" => string dataDir;
400.0 => float update; // update rate in ms

// new class to manage envelopes
class Env
{
  Step s => Envelope e => blackhole; // feed constant into env
  update::ms => e.duration; // set ramp time
  fun void target (float val) { e.target(val); }
  fun void value (float val) { e.value(val); }
}
Env gdpVal;
Env curVal;
"gdp" => string f;
"cur" => string f2;

fun DataReader data()
{
  DataReader tmp;
  tmp.setDataSource(dataDir + f + ".dat");
  tmp.start();
  return tmp;
}

fun DataReader data2()
{
  DataReader tmp;
  tmp.setDataSource(dataDir + f2 + ".dat");
  tmp.start();
  return tmp;
}

fun float amp(float v)
{
// <<<v>>>;
//    return Math.dbtorms(85.0 + 15.0*v);
    return v;
}

fun void gdpThread()
{
  data() @=> DataReader xxx;
  data2() @=> DataReader yyy;
  false => int quit;
  while (!quit)
  {
    gdpVal.target(xxx.scaledVal());
    curVal.target(yyy.scaledVal());
    update::ms => now;
    (xxx.isFinished() || yyy.isFinished())  => quit;
  }
  <<<"gdp / cur quit", now/second>>>;
}
spork ~ gdpThread();
/////////////////////////////////////////////////////////////////////////

FileIO fout;
fout.open( "score_mp.txt", FileIO.WRITE );
if( !fout.good() ) cherr <= "can't open file for writing..." <= IO.newline();

90.0 => float len;
fout <= "duration " <= len <= "\n";

class Osc
{
  SinOsc o => Gain g => blackhole;
  Step unity => g;
  g.gain(0.5);
}
Osc osc[3];
osc[0].o.freq(0.15); // x
osc[1].o.freq(0.155); // dur
osc[2].o.freq(0.16); // y

string plate;
1 => int i;
0 => int j;
now + len::second => time end;
0.001::second => now;

/////////////////////

  now => time endStroke;
  75.6::second +=> endStroke;
  while (now<=endStroke)
  {
    now/second => float beg;
    osc[0].g.last() => float x;
(gdpVal.e.last()*2)-1.0 => x;
    0.25 *=> x;
    0.7 +=> x;

    osc[2].g.last() => float y;
(gdpVal.e.last()*2)-1.0 => y;
    0.25 *=> y;
    0.7 +=> y;

    osc[1].g.last() => float dur;
((1.0 - gdpVal.e.last())*2)-1.0 => dur;
      0.0002 *=> dur;
      0.0004 +=> dur;
// <<<dur, now/second>>>;
    1 => int loud;
    0.005 => float max; // small hit
// 130 124 118 112 106
// j has 0 - 2 range
// 142 118
(curVal.e.last())*2 => float j; // inverted because of db scale
// 142 100
120+42-(j*15) => float db;
    Std.dbtorms(db) *=> max; // medium scrape is +10dB by comparison

    2000.0 *=> max;

    if (i%2) "plat1" => plate; else "plat2" => plate;
if (db > 0.0) 
    fout <= "strike " <= beg <= " " <= plate <= " " <= x <= " "  <= y <= " "  <= dur <= " "  <= max <= "\n";
    dur*10::second => now;
  }

end-now => now;

fout.close();
