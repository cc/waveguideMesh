// chuck -s --caution-to-the-wind alignLDV.ck

// one file for raster / waterfall

14 => int nChans;
SndBuf s[nChans];
Delay d[nChans];
FileIO fout;

for (0 => int i; i< nChans; i++) {
  i+1 => int a; // audacity numbering 1-based
  "0" => string tens;
  if (a>9) "" => tens;
  s[i].read("../ldv/"+a+".wav");
  s[i] => blackhole;
  d[i].delay(20::samp);
  fout.open("/tmp/plotWFS.dat", FileIO.WRITE);
}

int trig[nChans];
int trigSamp[nChans];
for (0 => int i; i< nChans; i++) {
  false => trig[i];
  0 => trigSamp[i];
}

for (0 => int i; i< nChans; i++) {
  4800 => int quit;
  s[i].pos(0);
  for (0 => int j; j<4800; j++) {
    s[i].last() => float trigVal;
    if (!trig[i] && (trigVal > 0.1) ) { 
      true => trig[i]; <<<i, j>>>; 
      j => trigSamp[i];
      j+280 => quit;
    }
    1::samp => now;
    if (j>quit) break;
  }
}

for (0 => int i; i< nChans; i++) {
  s[i].pos(trigSamp[i] + 43);
  s[i] => d[i] => blackhole;
}

for (0 => int j; j<280; j++) {
  for (0 => int i; i< nChans; i++) {
    s[i].last() => float val;
2.0 *=> val;
    fout <= val + "\t";
  }
  fout <= "\n";
  1::samp => now;
}

fout.close();

// Std.system ("gnuplot -e \"\" plotLDV.txt -persist");
 Std.system ("gnuplot -e \"\" plotVDVSVG.txt -persist");

