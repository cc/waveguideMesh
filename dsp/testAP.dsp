// faust2api -dummy testAP.dsp 
// search for
// virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {

import("stdfaust.lib");
// #### Usage
//
// ```
// _ : modeFilter(freq,t60,gain) : _
// ```
//
// Where:
//
// * `freq`: mode frequency
// * `t60`: mode resonance duration (in seconds)
// * `gain`: mode gain (0-1)
//----------------------------------
modeFilter(freq,t60,gain) = fi.tf2(b0,b1,b2,a1,a2)*gain
with{
	b0 = 1;
	b1 = 0;
	b2 = -1;
	w = 2*ma.PI*freq/ma.SR;
	r = pow(0.001,1/float(t60*ma.SR));
	a1 = -2*r*cos(w);
	a2 = r^2;
};

stretch = 0.75;
apFilter(f) = fi.tf2(b0,b1,b2,a1,a2)
with{
	coef1 = stretch * stretch;
	a2 = coef1;
        b0 = coef1;
	b2 = 1.0;

	w = 2*ma.PI*f/ma.SR;
	coef2 = -2*stretch*cos(w);
	a1 = coef2;
	b1 = coef2;
};

xxxN(N,freqOff,freq) = seq(i,N,apFilter(i*freqOff+freq));
process = fi.pole(0.04)*0.99999999999999 : xxxN(4,575,1000);

// process = fi.pole(0.04)*0.99999999999999;

// process = no.noise : apFilter(500);
