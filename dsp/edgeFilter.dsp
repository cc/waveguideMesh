// faust2api -dummy edgeFilter.dsp 
// faust2svg  edgeFilter.dsp
// search for
// virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {

import("stdfaust.lib");

stretchConst = 0.7;
pole = 0.04;
apFilter(f,in) = in : fi.tf2(b0,b1,b2,a1,a2)
with{
  sigDep1(z) = ba.if ((z<0.0), -0.002, 0.002);
  stretch = stretchConst + sigDep1(in)*in;
	coef1 = stretch * stretch;
	a2 = coef1;
        b0 = coef1;
	b2 = 1.0;

	w = 2*ma.PI*f/48000.0;
	coef2 = -2*stretch*cos(w);
	a1 = coef2;
	b1 = coef2;
};

// process = fi.pole(0.04) : apFilter(0*575+1000);

// problem: input is both to seq and to par
apfN(N,freqOff,freq) = _ : seq(i,N,apFilter(i*freqOff+freq));
process = fi.pole(pole) : *(1-pole) : apfN(4,575,1000);

// process = fi.pole(0.04)*0.99999999999999;

// process = no.noise : apFilter(500);
