<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="asdf2html.xsl"?>
<asdf xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="asdf.xsd">
  <header>
    <name>Loudspeaker Bar</name>
  </header>

  <reproduction_setup>
    <linear_array number="15" name="linear segment front left">
      <first>
        <position x="1.0" y="0.35556"/>
        <orientation azimuth="180"/>
      </first>
      <second>
        <position x="1.0" y="0.30476"/>
      </second>
    </linear_array>

  </reproduction_setup>
</asdf>
