<?xml version="1.0" encoding="utf-8"?>

<asdf
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="asdf.xsd"
  version="0.1">

  <header>
    <name>12 live inputs</name>
    <description>
      This scene creates 12 sound sources and connects them
      to the first 12 inputs of your sound card (if available).
    </description>
  </header>

  <scene_setup>

    <source name="live input 1" model="point">
      <port>1</port>
      <position x="-0.1380" y="1.0100"/>
    </source>

    <source name="live input 2" model="point">
      <port>2</port>
      <position x="-0.1150" y="1.0100"/>
    </source>

    <source name="live input 3" model="point">
      <port>3</port>
      <position x="-0.0920" y="1.0100"/>
    </source>

    <source name="live input 4" model="point">
      <port>4</port>
      <position x="-0.0690" y="1.0100"/>
    </source>

    <source name="live input 5" model="point">
      <port>5</port>
      <position x="-0.0460" y="1.0100"/>
    </source>

    <source name="live input 6" model="point">
      <port>6</port>
      <position x="-0.0230" y="1.0100"/>
    </source>

    <source name="live input 7" model="point">
      <port>7</port>
      <position x="0.0000" y="1.0100"/>
    </source>

    <source name="live input 8" model="point">
      <port>8</port>
      <position x="0.0230" y="1.0100"/>
    </source>

    <source name="live input 9" model="point">
      <port>9</port>
      <position x="0.0460" y="1.0100"/>
    </source>

    <source name="live input 10" model="point">
      <port>10</port>
      <position x="0.0690" y="1.0100"/>
    </source>

    <source name="live input 11" model="point">
      <port>11</port>
      <position x="0.0920" y="1.0100"/>
    </source>

    <source name="live input 12" model="point">
      <port>12</port>
      <position x="0.1150" y="1.0100"/>
    </source>

  </scene_setup>
</asdf>
