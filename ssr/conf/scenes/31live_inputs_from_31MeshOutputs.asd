<?xml version="1.0" encoding="utf-8"?>

<asdf
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="asdf.xsd"
  version="0.1">

  <header>
    <name>31 live inputs</name>
    <description>
      This scene creates 31 sound sources and connects them
      to the first 31 inputs of your sound card (if available).
    </description>
  </header>

  <scene_setup>

    <source name="live input 1" model="point">
      <port>1</port>
      <position x="-0.3175" y="1.0100"/>
    </source>

    <source name="live input 2" model="point">
      <port>2</port>
      <position x="-0.2963" y="1.0100"/>
    </source>

    <source name="live input 3" model="point">
      <port>3</port>
      <position x="-0.2752" y="1.0100"/>
    </source>

    <source name="live input 4" model="point">
      <port>4</port>
      <position x="-0.2540" y="1.0100"/>
    </source>

    <source name="live input 5" model="point">
      <port>5</port>
      <position x="-0.2328" y="1.0100"/>
    </source>

    <source name="live input 6" model="point">
      <port>6</port>
      <position x="-0.2117" y="1.0100"/>
    </source>

    <source name="live input 7" model="point">
      <port>7</port>
      <position x="-0.1905" y="1.0100"/>
    </source>

    <source name="live input 8" model="point">
      <port>8</port>
      <position x="-0.1693" y="1.0100"/>
    </source>

    <source name="live input 9" model="point">
      <port>9</port>
      <position x="-0.1482" y="1.0100"/>
    </source>

    <source name="live input 10" model="point">
      <port>10</port>
      <position x="-0.1270" y="1.0100"/>
    </source>

    <source name="live input 11" model="point">
      <port>11</port>
      <position x="-0.1058" y="1.0100"/>
    </source>

    <source name="live input 12" model="point">
      <port>12</port>
      <position x="-0.0847" y="1.0100"/>
    </source>

    <source name="live input 13" model="point">
      <port>13</port>
      <position x="-0.0635" y="1.0100"/>
    </source>

    <source name="live input 14" model="point">
      <port>14</port>
      <position x="-0.0423" y="1.0100"/>
    </source>

    <source name="live input 15" model="point">
      <port>15</port>
      <position x="-0.0212" y="1.0100"/>
    </source>

    <source name="live input 16" model="point">
      <port>16</port>
      <position x="0.0000" y="1.0100"/>
    </source>

    <source name="live input 17" model="point">
      <port>17</port>
      <position x="0.0212" y="1.0100"/>
    </source>

    <source name="live input 18" model="point">
      <port>18</port>
      <position x="0.0423" y="1.0100"/>
    </source>

    <source name="live input 19" model="point">
      <port>19</port>
      <position x="0.0635" y="1.0100"/>
    </source>

    <source name="live input 20" model="point">
      <port>20</port>
      <position x="0.0847" y="1.0100"/>
    </source>

    <source name="live input 21" model="point">
      <port>21</port>
      <position x="0.1058" y="1.0100"/>
    </source>

    <source name="live input 22" model="point">
      <port>22</port>
      <position x="0.1270" y="1.0100"/>
    </source>

    <source name="live input 23" model="point">
      <port>23</port>
      <position x="0.1482" y="1.0100"/>
    </source>

    <source name="live input 24" model="point">
      <port>24</port>
      <position x="0.1693" y="1.0100"/>
    </source>

    <source name="live input 25" model="point">
      <port>25</port>
      <position x="0.1905" y="1.0100"/>
    </source>

    <source name="live input 26" model="point">
      <port>26</port>
      <position x="0.2117" y="1.0100"/>
    </source>

    <source name="live input 27" model="point">
      <port>27</port>
      <position x="0.2328" y="1.0100"/>
    </source>

    <source name="live input 28" model="point">
      <port>28</port>
      <position x="0.2540" y="1.0100"/>
    </source>

    <source name="live input 29" model="point">
      <port>29</port>
      <position x="0.2752" y="1.0100"/>
    </source>

    <source name="live input 30" model="point">
      <port>30</port>
      <position x="0.2963" y="1.0100"/>
    </source>

    <source name="live input 31" model="point">
      <port>31</port>
      <position x="0.3175" y="1.0100"/>
    </source>

  </scene_setup>
</asdf>
