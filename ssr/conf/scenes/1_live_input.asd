<?xml version="1.0" encoding="utf-8"?>

<asdf
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="asdf.xsd"
  version="0.1">

  <header>
    <name>1 live input</name>
    <description>
      This scene creates 1 sound source and connects it
      to the first input of your sound card (if available).
    </description>
  </header>

  <scene_setup>

    <source name="live input 1" model="point">
      <port>1</port>
      <position x="-0.17745" y="1.025"/>
    </source>

  </scene_setup>
</asdf>
