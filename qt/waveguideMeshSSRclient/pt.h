#ifndef PT_H
#define PT_H
#define MATRIX_SIZE 4 // rectangular mesh of 4 nodes / sides per cell

class Pt
{
public:
    Pt();
    void xyNormalized(float x, float y);
    void xyAbsolute(float x, float y);
    void xyNormalizedMaxX(float x, float y, int maxX);
//// r0
//    int llx(){ return mlx; } //mlx
//    int lly(){ return mly; } //mly
//// r1
//    int ulx(){ return mlx; } //mlx
//    int uly(){ return muy; } //muy
//// r2
//    int urx(){ return mrx; } //mrx
//    int ury(){ return muy; } //muy
//// r3
//    int lrx(){ return mrx; } //mrx
//    int lry(){ return mly; } //mly

//    float llf(){ return mllf; }
//    float ulf(){ return mulf; }
//    float urf(){ return murf; }
//    float lrf(){ return mlrf; }

    int sides;
    int rx[MATRIX_SIZE];
    int ry[MATRIX_SIZE];
    float s[MATRIX_SIZE];
    float mx;
    float my;
    float mPiOverTwo; // classic panning
    float mxPan;
    float myPan;
private:
    void rectangleAround(float x, float y);
    float fullSwingScaledToDimension(float v, int r);
};

#endif // PT_H
