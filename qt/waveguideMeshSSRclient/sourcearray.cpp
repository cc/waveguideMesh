#include "sourcearray.h"
#include <math.h>
#include <QDebug>

// multichannel source for Soundscape Renderer

SourceArray::SourceArray() : xOff(0.15), xInc(0.052),  // xInc(0.029), xInc(0.058),
    yOff(0.1), yInc(0.01),
    fixedArray(false)
{
    setPhasors(3);
    update(fixedArray);
}

void SourceArray::toggleFixedArray()
{
    fixedArray = !fixedArray;
}

void SourceArray::setPhasors(int which)
{
    int tmp = which;
    if (tmp < 7)
        for (int i=0; i<nChans; i++) phasor[i] = 0.0;
    else tmp -= 10;
    for (int i=0; i<nChans; i++)
    {
        switch (tmp) {
        case 1:
            inc[i] = nChans * 0.01;         // one group
            break;
        case 2:
            inc[i] = (nChans-i/5) * 0.005;   // two groups
            break;
        case 3:
            inc[i] = (nChans-i%5) * 0.1;   // five groups
            break;
        case 4:
            inc[i] = (nChans-i) * 0.005;   // nChans groups
            break;
        case 5:
            inc[i] = (nChans-i) * -0.002;   // nChans groups
            break;
        case 6:
            inc[i] = i*3 * -0.001;   // nChans groups
            break;
        default:
            qDebug() << "unimplemented";
            break;
        }
    }
    if (which < 7)
        for (int i=0; i<1000; i++)
            for (int i=0; i<nChans; i++)
                phasor[i] += inc[i];
}

void SourceArray::setX(double x)
{
    xOff = x;
    update(fixedArray);
}

void SourceArray::setY(double y)
{
    yOff = y;
    update(fixedArray);
}

void SourceArray::moveX(double dX)
{
    xOff -= dX;
    update();
}

void SourceArray::moveY(double dY)
{
    yOff += dY;
    update();
}

void SourceArray::update(bool fixedArray)
{
    if (!fixedArray) {
        for (int i=0; i<nChans; i++)
        {
            phasor[i] += inc[i];
            x[i] = 1.5*sin(xInc*phasor[i]);
            //            qDebug() << yOff;
            y[i] = cos(yInc*phasor[i]);
        }
    } else { // track [0] and keep spacing
        phasor[0] += inc[0];
        double xTmp = 1.5*sin(xInc*phasor[0]);
        double yTmp = cos(yInc*phasor[0]);
        for (int i=0; i<nChans; i++)
        {
            x[i] = xOff + i*xInc + xTmp;
            y[i] = yOff + i*yInc + yTmp;
        }
    }
}

double SourceArray::getX(int i)
{
    return x[i];
}

double SourceArray::getY(int i)
{
    // don't cross below 0.5
    if (y[i] < 0.5) y[i] = 0.5 - (y[i] - 0.5);
    return y[i];
}
