#include "fileio.h"

FileIO::FileIO()
{

}

void FileIO::open(QString path)
{
    file = new QFile(path);
    // Trying to open in WriteOnly and Text mode
    if(!file->open(QFile::WriteOnly |
                   QFile::Text))
    {
        qDebug() << " Could not open file for writing";
        return;
    }
}

void FileIO::writeLine(QString line)
{
    QTextStream out(file);
    out << line;

}

void FileIO::writeDouble(double val)
{
    writeLine("\t"+QString::number(val)+"\n");

}

void FileIO::writeNextColumn(double val)
{
    writeLine(QString::number(val)+"\t");

}

void FileIO::endLine()
{
    writeLine("\n");

}

void FileIO::close()
{
    file->flush();
    file->close();
}

