#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include <QLoggingCategory>
#include <QTimer>
#include <sched.h>  // sched_setaffinity
#include "wavfileio.h"
#include "striker.h"
#include "jackaudiointerface.h"
#include "globals.h"
void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    fprintf(stderr, "%s\n", localMsg.constData());
    fflush(stderr);
}

int main(int argc, char *argv[])
{
    //        int cpuAffinity = argc > 1 ? atoi(argv[1]) : 2; // force to CPU 0 by default

    //        if (cpuAffinity > -1)
    //        {
    //            cpu_set_t mask;
    //            int status;

    //            CPU_ZERO(&mask);
    //            CPU_SET(cpuAffinity, &mask);
    //            status = sched_setaffinity(0, sizeof(mask), &mask);
    //            if (status != 0)
    //            {
    //                perror("sched_setaffinity");
    //            }
    //        }
    QApplication a(argc, argv);
    // w/ mouse events
    //    QLoggingCategory::setFilterRules(QStringLiteral("*.debug=true"));
    QLoggingCategory::setFilterRules("*.debug=true\nqt.*.debug=false");
    qInstallMessageHandler(myMessageOutput);
    MainWindow w;

    Stk::setSampleRate(FS);
    qDebug() << "plate = " << nX << "by" << nY << "cbrass m/s" << cbrass << "spatial sample" << mPerSampBrass;
#ifdef STKMESH
    qDebug() << "Mesh2D version";
#else
    qDebug() << "waveguideMesh version";
    GenMesh mesh;
#endif

//     #define RT 1
#ifdef RT
    Striker s;
    w.setStriker(&s);
    w.show(); // connects to Striker after connecting to SSR to relay wiggles
    JackAudioInterface rt(4, N_OUTCHANS);
    rt.setStriker(&s);
    rt.setMesh(&mesh);
    s.start();
    rt.start();
    a.exec();
#else
    WavFileIO wav(1, N_OUTCHANS);
#ifdef STKMESH
#else
    wav.setMesh(&mesh);
#endif
    wav.start();
    //    QTimer::singleShot(1500, &a,  SLOT(quit()));
#endif
    return 1;
}
