#ifndef WAVFILEIO_H
#define WAVFILEIO_H
#include "fileio.h"
#include "genmesh.h"
#include <QObject>

class WavFileIO : public FileIO
{
public:
    WavFileIO(int I, int O);
    void setMesh(GenMesh *m) { mesh = m; }
    void start();
private:
    int nI;
    int nO;
    GenMesh *mesh;

};

#endif // WAVFILEIO_H
