#ifndef GENMESH_H
#define GENMESH_H
#include <QVarLengthArray>
#include "stk/Instrmnt.h"
#include "stk/OnePole.h"
#include "stk/OneZero.h"
#include "stk/BiQuad.h"
#include "stk/DelayL.h"
#include <QObject>
#include "stk/PoleZero.h"
#include "pt.h"
using namespace stk;
#include "workerthread.h"

class GenMesh : public QObject , public Instrmnt
{
    Q_OBJECT
public:
    GenMesh();
    ~GenMesh( );
    //! Reset and clear all internal state.
    void clear( void );

    //! Set the x dimension size in samples.
    void setNX( unsigned short lenX );

    //! Set the y dimension size in samples.
    void setNY( unsigned short lenY );

    //    //! Set the z dimension size in samples.
    //    void setNZ( unsigned short lenZ );

    //! Set the x, y input position on a -1.0 - 1.0 scale.
    void setInputPosition( float xFactor, float yFactor );

    //! Set the x, y output position on a -1.0 - 1.0 scale.
    void mapOutputPositions( float xFactor0, float yFactor0,
                             float xFactor1, float yFactor1 ,
                             bool adj = false);

    //    //! Set the loss filters gains (0.0 - 1.0).
    //    void setDecay( float decayFactor );

    //! Impulse the mesh with the given amplitude (frequency ignored).
    void noteOn( StkFloat frequency, StkFloat amplitude );

    //! Stop a note with the given amplitude (speed of decay) ... currently ignored.
    void noteOff( StkFloat amplitude );


    // unused Instrmnt class member functions
    void controlChange( int number, float value ){}

    StkFloat tick( unsigned int channel = 0 ){}

    StkFrames& tick( StkFrames& frames, unsigned int channel = 0 ){}
//    unsigned short NX_, NY_, nOut;
#define float2D(arr) QVarLengthArray <QVarLengthArray <float> > arr
    float **xp;
    float **xm;
    float **yp;
    float **ym;
    float **xpz;
    float **xmz;
    float **ypz;
    float **ymz;
    float **vs;
    float **in;
    float *out;
    float **prox;
    float **probe;
    float** floatVect( int x, int y);
    //! Set the stretch "factor" of the string (0.0 - 1.0).
    float setStretch( float in, float off, float sclNeg, float sclPos );
    double setStretchbiquadY( float in, float stretchOff, float stretchSclNeg, float stretchSclPos, int i, int o );
    void setStretchbiquadX( float in, float stretchOff, float stretchSclNeg, float stretchSclPos, int i, int o );
    OnePole  *filterX_;
    OnePole  *filterY_;
    OnePole  *filterX2_;
    OnePole  *filterY2_;
    BiQuad  **biquadX_;
    BiQuad  **biquadY_;
    int counter_;
    QVarLengthArray<PoleZero*> dcBlocker;
    QVarLengthArray<OnePole*> onePoleOutput;
    float mStrikeVal;
    float tickStrikeVal(float val);

    //! test edge filter IR.
    StkFrames& inputTickX( float input );
    float leftEdgeX(int beg = 0, int end = 0 , float in = 0.0);

    //! Input a sample to the mesh and compute one output sample.
    StkFrames& inputTick( float input );
    void leftEdge(int beg = 0, int end = 0 );
    void rightEdge( int beg = 0, int end = 0 );
    void bottomEdge( int beg = 0, int end = 0 );
    void topEdge( int beg = 0, int end = 0 );
    QVarLengthArray<WorkerThread*> workerThreads;
    void joinWorkerThreads();
    QVarLengthArray<Pt> inpt;
    QVarLengthArray<Pt> dampt;
    QVarLengthArray<Pt> outpt;
    int MAX_OUTCHANS;
private:
    double fscl;
    double freq[4];
    DelayL delay;
    float logmap(float x, float r);
private slots:
    void setStrikeVal(double v);
};

#endif // GENMESH_H
