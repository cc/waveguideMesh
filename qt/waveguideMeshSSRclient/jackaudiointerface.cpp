#include "jackaudiointerface.h"

#include <cstdlib>
#include <cstring>
#include <cmath>
#include <stdexcept>
#include <QObject>
#include <QDebug>
using namespace stk;

JackAudioInterface::JackAudioInterface(int I, int O) :
     nI(I),nO(O)
{
    mInBuffer.resize(nI);
    mOutBuffer.resize(nO);
}
//*******************************************************************************
JackAudioInterface::~JackAudioInterface()
{}

//*******************************************************************************
//jack_port_t *input_port;
//jack_port_t *output_port;

int
JackAudioInterface::processCallback (jack_nframes_t nframes, void *arg)
{
    //    jack_default_audio_sample_t *in, *out;
    //    in = (jack_default_audio_sample_t *)jack_port_get_buffer (input_port, nframes);
    //    out =(jack_default_audio_sample_t *) jack_port_get_buffer (output_port, nframes);

    // test straight wire memcpy
    //    memcpy (out, in, sizeof (jack_default_audio_sample_t) * nframes);
    // test straight wire loops
    //        for (int i=0; i<nframes; i++ ) {
    //            for (int j=0; j<2; j++ ) {
    //                *out++ = *in++;
    //            }
    //        }
    for (int i = 0; i < nI; i++) {
        // Input Ports are READ ONLY
        mInBuffer[i] = (sample_t*) jack_port_get_buffer(mInPorts[i], nframes);
    }
    for (int i = 0; i < nO; i++) {
        // Output Ports are WRITABLE
        mOutBuffer[i] = (sample_t*) jack_port_get_buffer(mOutPorts[i], nframes);
    }
    //qDebug() << nI << nO;

    // straightwire fan out _ <: _,_
    //    for (int f=0; f<nframes; f++ ) {
    //        for (int inCh=0; inCh<nI; inCh++ ) {
    //            sample_t* in_sample = mInBuffer[inCh]; //sample buffer for channel i
    //            for (int outCh=0; outCh<nO; outCh++ ) {
    //                sample_t* out_sample = mOutBuffer[outCh]; //sample buffer for channel i
    //                out_sample[f] = in_sample[f];
    //            }
    //        }
    //    }
    sample_t* in_chanBuffer = mInBuffer[0]; //sample buffer for channel i
    sample_t* stretchBuffer = mInBuffer[1]; // time-varying stretch channel /////////
    sample_t* xposBuffer = mInBuffer[2]; //sample buffer for channel i
    sample_t* yposBuffer = mInBuffer[3]; //sample buffer for channel i
    for (int f=0; f<nframes; f++ ) {
        mMesh->inpt[0].xyNormalized( xposBuffer[0], yposBuffer[1] );

//        if(f==0) mMesh->setStretch(stretchBuffer[f]); // time-varying stretch channel /////////

        StkFrames outFrameM;
        if (mStriker->synth) {
            outFrameM = mMesh->inputTick(in_chanBuffer[f]);
        }
        for (int outCh=0; outCh<nO; outCh++ ) {
            sample_t* out_chanBuffer = mOutBuffer[outCh]; //sample buffer for channel i
            out_chanBuffer[f] = (mStriker->synth) ? outFrameM[outCh] : 0.0;
        }
//        mStriker->x = mPosX;
//        mStriker->y = mPosY;
    }

    return 0;
}

int JackAudioInterface::processCallbackWrapper(jack_nframes_t nFrames, void *arg)
{
    return static_cast<JackAudioInterface*>(arg)->processCallback(nFrames, arg);
}

void
jack_shutdown (void *arg)
{
    exit (1);
}

void JackAudioInterface::setupClient()
{
    const char **ports;
    const char *client_name = "mesh5000";
    const char *server_name = NULL;
    jack_options_t options = JackNullOption;
    jack_status_t status;

    mClient = jack_client_open (client_name, options, &status, server_name);

    if (mClient == NULL) {
        fprintf (stderr, "jack_client_open() failed, "
                         "status = 0x%2.0x\n", status);
        if (status & JackServerFailed) {
            fprintf (stderr, "Unable to connect to JACK server\n");
        }
        exit (1);
    }
    if (status & JackServerStarted) {
        fprintf (stderr, "JACK server started\n");
    }
    if (status & JackNameNotUnique) {
        client_name = jack_get_client_name(mClient);
        fprintf (stderr, "unique name `%s' assigned\n", client_name);
    }
    jack_set_process_callback (mClient, &JackAudioInterface::processCallbackWrapper, this);

    // Set function to call if Jack shuts down
    jack_on_shutdown (mClient, this->jackShutdown, 0);

    fprintf (stderr,"engine sample rate: %" PRIu32 "\n",
             jack_get_sample_rate (mClient));

    /* create two ports */

    //Create Input Ports
    mInPorts.resize(nI);
    for (int i = 0; i < nI; i++)
    {
        QString inName;
        QTextStream (&inName) << "in_" << i+1;
        mInPorts[i] = jack_port_register (mClient, inName.toLatin1(),
                                          JACK_DEFAULT_AUDIO_TYPE,
                                          JackPortIsInput, 0);
    }
    //    input_port = jack_port_register (mClient, "input",
    //                                     JACK_DEFAULT_AUDIO_TYPE,
    //                                     JackPortIsInput, 0);

    //Create Output Ports
    mOutPorts.resize(nO);
    for (int i = 0; i < nO; i++)
    {
        QString outName;
        QTextStream (&outName) << "out_" << i+1;
        mOutPorts[i] = jack_port_register (mClient, outName.toLatin1(),
                                           JACK_DEFAULT_AUDIO_TYPE,
                                           JackPortIsOutput, 0);
    }
    //    output_port = jack_port_register (mClient, "output",
    //                                      JACK_DEFAULT_AUDIO_TYPE,
    //                                      JackPortIsOutput, 0);

    //    if ((input_port == NULL) || (output_port == NULL)) {
    //        fprintf(stderr, "no more JACK ports available\n");
    //        exit (1);
    //    }

    /* Tell the JACK server that we are ready to roll.  Our
     * process() callback will start running now. */

    if (jack_activate (mClient)) {
        fprintf (stderr, "cannot activate client");
        exit (1);
    }


    ports = jack_get_ports (mClient, NULL, NULL,
                            JackPortIsPhysical|JackPortIsOutput);
    if (ports == NULL) {
        fprintf(stderr, "no physical capture ports\n");
        exit (1);
    }

    //    for (int i = 0; i < nI; i++)
    //        if (jack_connect (mClient, ports[0], jack_port_name (mInPorts[i]))) {
    //            fprintf (stderr, "cannot connect input ports\n");
    //        }

    free (ports);

    ports = jack_get_ports (mClient, NULL, NULL,
                            JackPortIsPhysical|JackPortIsInput);
    if (ports == NULL) {
        fprintf(stderr, "no physical playback ports\n");
        exit (1);
    }

    //    for (int i = 0; i < nO; i++)
    //        if (jack_connect (mClient, jack_port_name (mOutPorts[i]), ports[i])) {
    //            fprintf (stderr, "cannot connect output ports\n");
    //        }
    free (ports);
}

//*******************************************************************************
void JackAudioInterface::jackShutdown (void*)
{
    //std::cout << "The Jack Server was shut down!" << std::endl;
    throw std::runtime_error("The Jack Server was shut down!");
    //std::cout << "Exiting program..." << std::endl;
    //std::exit(1);
}

void JackAudioInterface::setMesh(GenMesh *m)
{
    mMesh = m;
}

void JackAudioInterface::setCollision(double x, double y)
{
//    nXc = x;
//    nYc = y;
//    mCollision = new GenMesh(nXc,nYc,nO,FS,false);
//    mCollision->mapOutputPositions(-1.0,0.0,1.0,0.0);
//    mCollision->setStretch(0.5);
}

void JackAudioInterface::setStriker(Striker *s){
    mStriker = s;
    bool rtn = connect(mStriker,SIGNAL(strikeAudio(double)),mMesh,SLOT(setStrikeVal(double)));
//    qDebug() << "================" << rtn;
}
void JackAudioInterface::run()
{
    setupClient();
    while (true)
    {
        msleep(100);
    }

}

