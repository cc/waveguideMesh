#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QObject>
#include <QThread>
#include <sched.h>  // sched_setaffinity
#include <QDebug>
#include <QMutex>
#include <QMutexLocker>

class WorkerThread : public QThread
{
    Q_OBJECT
public:
    WorkerThread(QObject *mesh, int id, int n, int x, int y);
    void tick();
    void run() override {
        //        int cpuAffinity = 3;
        //        cpu_set_t mask;
        //        int status;

        //        CPU_ZERO(&mask);
        //        CPU_SET(cpuAffinity, &mask);
        //        status = sched_setaffinity(0, sizeof(mask), &mask);
        //        if (status != 0)
        //        {
        //            qDebug() << "sched_setaffinity";
        //        }
        //        exec(); signals with eventloop won't allow separate thread
        while (!done) {
            if (isGo()) edges();
            yieldCurrentThread();
        }
    }
    bool isWaiting() {
        return waiting;
    }
    bool done;
private:
    void setGo(bool b) {
        go = b;
    }
    bool isGo() {
        return go;
    }
    void setWaiting(bool b) {
        waiting = b;
    }
    QMutex mMutex;
    bool waiting;
    bool go;
    QObject *mesh;
    int myID;
    int nWorkers;
    int nX;
    int nY;
    int xBeg;
    int xEnd;
    int yBeg;
    int yEnd;
    void edges();
};

#endif // WORKERTHREAD_H
