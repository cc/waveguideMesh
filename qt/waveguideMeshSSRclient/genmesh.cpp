﻿#include "genmesh.h"
#include <math.h>
#include <QDebug>
#include "globals.h"

using namespace stk;
//#define WORKERS 3

GenMesh :: GenMesh()
{
    this->setNX( nX );
    this->setNY( nY );
    counter_ = 0; // unused

    lastFrame_.resize( 1, N_OUTCHANS, 0.0 ); // resize for multi-channel out

    xp = floatVect(nX,nY);
    xm = floatVect(nX,nY);
    yp = floatVect(nX,nY);
    ym = floatVect(nX,nY);
    xpz = floatVect(nX,nY);
    xmz = floatVect(nX,nY);
    ypz = floatVect(nX,nY);
    ymz = floatVect(nX,nY);
    vs = floatVect(nX,nY);
    in = floatVect(nX,nY);
    //    out = floatVect(nX,nY);
    out = new float [nX]();
    prox = floatVect(nX,N_OUTCHANS);
    probe = floatVect(nX,nY);
    filterX_ = new OnePole[nX];
    filterY_ = new OnePole[nY];
    filterX2_ = new OnePole[nX];
    filterY2_ = new OnePole[nY];
    biquadX_ = new BiQuad* [nX]; for(int i = 0; i < nX; i++) biquadX_[i] = new BiQuad[4]();
    biquadY_ = new BiQuad* [nY]; for(int i = 0; i < nY; i++) biquadY_[i] = new BiQuad[4]();
    this->clear();
    //    StkFloat pole = 0.05; // 0.1; // 0.05
    //    StkFloat gain = 0.999; // celletto  // 0.9999999 strikes // 0.9997 -- 1st damping // 59x42 exs 0.997
    for (int i=0; i<nY; i++ ) {
        filterY_[i].setPole( pole );
        filterY_[i].setGain( decay );
    }
    for (int i=0; i<nX; i++ ) {
        filterX_[i].setPole( pole );
        filterX_[i].setGain( decay );
    }
    for (int i=0; i<nY; i++ ) {
        filterY2_[i].setPole( pole );
        filterY2_[i].setGain( decay );
    }
    for (int i=0; i<nX; i++ ) {
        filterX2_[i].setPole( pole );
        filterX2_[i].setGain( decay );
    }
    this->clear();
    dcBlocker.resize(N_OUTCHANS);
    for (int i = 0; i < N_OUTCHANS; i++) {
        dcBlocker[i] = new PoleZero();
        dcBlocker[i]->setBlockZero();
    }
    onePoleOutput.resize(N_OUTCHANS);
    for (int i = 0; i < N_OUTCHANS; i++) {
        onePoleOutput[i] = new OnePole();
        onePoleOutput[i]->setPole( 0.15f );
//        onePoleOutput[i]->setGain( 2.0f );
    }
#ifdef WORKERS
    workerThreads.resize(WORKERS);
    for (int i = 0; i < WORKERS; i++) {
        workerThreads[i] = new WorkerThread(this, i, WORKERS, nX, nY);
        //        startWorkInAThread(workerThreads[i]);
        workerThreads[i]->start();
    }
#else
#endif

    inpt.resize(N_INPTS);
    for (int i = 0; i < N_INPTS; i++) {
        inpt[i].xyNormalized( -1.0, -1.0 );
        qDebug() << "default input in constructor set to near corner"
                 << "-- x" << inpt[i].mx
                 << "y" << inpt[i].my;
    }

    dampt.resize(N_DAMPS);
    for (int i = 0; i < N_DAMPS; i++) {
        dampt[i].xyNormalized( ((i)?-0.77:0.77), -0.0 );
        qDebug() << "default damp pt in constructor set to middle"
                 << "-- x" << dampt[i].mx
                 << "y" << dampt[i].my;
    }

    outpt.resize(N_OUTCHANS);

    float xFactor1 = 1.0;
    float xFactor0 = -1.0;
    float yFactor1 = 0;
    float yFactor0 = 0;
    MAX_OUTCHANS = N_OUTCHANS;
#ifdef FIXED_SPACING
    float xLength = X; // m
    float xAperture = (N_OUTCHANS - 1) * FIXED_SPACING; // m
    float xStart = 1.0f; // node
    float outputSpacingNodes = FIXED_SPACING / mPerSampBrass; // (nX - xStart) / N_OUTCHANS;
    if (xAperture > xLength) qDebug() << "direct drive not possible, use ssr";
    qDebug() << xLength << xAperture << outputSpacingNodes ;
    for (int i = 0; i < N_OUTCHANS; i++) {
        float x = xStart + (i * outputSpacingNodes);
        float y = HORIZ_MIDLINE;
        outpt[i].xyAbsolute( x, y );
        if ( x > nX ) {
            MAX_OUTCHANS = i-1;
            qDebug() << i << "!! x" << x << nX << outpt[i].mx << "y" << outpt[i].my;
            break;
        } else {
            qDebug() << i << "-- x" << x << nX << outpt[i].mx << "y" << outpt[i].my;
        }
    }
#else
    for (int i = 0; i < N_OUTCHANS; i++) {
        float xRange = (xFactor1 - xFactor0);
        float xFrac = (float)(i+1)/(float)(N_OUTCHANS+1);
        xFrac *= xRange;
        xFrac += xFactor0;
        float yRange = (yFactor1 - yFactor0);
        float yFrac = (float)(i+1)/(float)(N_OUTCHANS+1);
        yFrac *= yRange;
        yFrac += yFactor0;
#ifdef FORCE_TO_EDGES
        if(xFrac < 0) xFrac = -1.0;
        if(xFrac > 0) xFrac = 0.9;
#endif
        outpt[i].xyNormalized( xFrac, yFrac );
#ifdef FORCE_OUTPUT_CORNER
        outpt[i] = new Pt (forceX,forceY);
#endif
        if(i>0) qDebug() << cair * (outpt[i].mx-outpt[i-1].mx) / 48000.0;
        qDebug() << i << "-- x" << xFrac << nX << outpt[i].mx << "y" << outpt[i].my;
    }
#endif

    for (int i=0; i<nX; i++ ) {
        for (int j = 0; j < N_OUTCHANS; j++) {
            float tmp = fabs(outpt[j].mx - (float)i);
            tmp += 1.0f;
            tmp = 1.0f / tmp;
            tmp *= tmp;
            prox[i][j] = 80.0f * tmp;
        }
    }

    /////////////////////// stretch frequencies
    fscl = TWO_PI / Stk::sampleRate();
    // #define MAGIC 0.025 in most tests and orignally 0.25 in code this came from
    { // oddball from Stk
        StkFloat lastFrequency_ = 500.0; // guess
        StkFloat tmp = lastFrequency_ * 2.0;
        StkFloat dFreq = ( (0.5 * Stk::sampleRate()) - tmp ) * MAGIC;
        for (int i = 0; i < 4; i++) {
            freq[i] =  tmp;
            tmp += dFreq;
            //            qDebug() << tmp << dFreq;
            for(int y = 0; y < nY; y++) biquadY_[y][i].setB2( 1.0 );
            for(int x = 0; x < nX; x++) biquadX_[x][i].setB2( 1.0 );
        }
    }
    delay.setDelay(66.0);
}

void GenMesh :: clear( void )
{
    for (int y=0; y<nY; y++ )
    {
    }
    for (int x=0; x<nX; x++ )
    {
    }
}

#define sum(x,y) vs[x][y] = 0.5f * (\
    xp [x]  [y] + \
    xm [x+1][y] + \
    yp [x]  [y] + \
    ym [x]  [y+1] \
    )

void GenMesh::setStrikeVal(double v){
    mStrikeVal = v;
    //    qDebug() << "yay" << v;
}

float GenMesh::tickStrikeVal( float val )
{
    return val;
}

StkFrames& GenMesh::inputTickX( float input )
{
    float out = leftEdgeX(0, 0, input);
    for(int j = 0; j < N_OUTCHANS; j++)
    {
        lastFrame_[j] =  out;
    }
    return lastFrame_;
}

#define LOGMAP(x,r) (r*x*(1.0f-x))
float GenMesh::logmap(float x, float r)
{
    x += 1.0f;
    x *= 0.5f;
    x = LOGMAP(x,r);
    x *= 2.0f;
    x -= 1.0f;
    return x;
}
StkFrames& GenMesh::inputTick( float input )
{

    // mute the mesh
    //    for (int x=0; x<nX-1; x++) {
    //        for (int y=0; y<NY_-1; y++) {
    //            vs[x][y] = 0.0;
    //        }
    //    }

    for (int x=0; x<nX; x++) { // zero out input plane
        for (int y=0; y<nY; y++) {
            in[x][y] = 0.0;
        }
    }

    for (int x=0; x<nX-1; x++) {
        for (int y=0; y<nY-1; y++) {
            vs[x][y] = sum(x,y);
        }
    }

    float tmp1 = input*EXC_GAIN;
    //    if (tmp1 > 1.0) tmp1 = 1.0;
    //    if (tmp1 < -1.0) tmp1 = -1.0;
    for (int i = 0; i < N_INPTS; i++) { // matrixify input signal
        for (int j = 0; j < inpt[i].sides; j++) {

            // add matrixified input to input plane
            float tmp2 = inpt[i].s[j]*tmp1;
            in[ inpt[i].rx[j] ] [ inpt[i].ry[j] ] += tmp2;

            // and add to plate
            vs[ inpt[i].rx[j] ] [ inpt[i].ry[j] ] += tmp2;

        }
        //        qDebug() << "inpt" << inpt[i].llx() << inpt[i].lly();
    }

    // damping points
    for (int i = 0; i < N_DAMPS; i++) { // matrixify damping points
        for (int j = 0; j < dampt[i].sides; j++) {

            // apply damp matrix to plate
            vs[ dampt[i].rx[j] ] [ dampt[i].ry[j] ]  *= CONTACT_DAMPING;

        }
        //        qDebug() << "inpt" << inpt[i].llx() << inpt[i].lly();
    }

    //#define R 3.9f
    //#define STATE_GAIN 0.01f
    //#define STATE_BOUNDS_NL 0.99f
    //    float current = vs[inpt[i].llx()][inpt[i].lly()] * STATE_GAIN;
    //    current += vs[inpt[i].ulx()][inpt[i].uly()] * STATE_GAIN;
    //    current += vs[inpt[i].urx()][inpt[i].ury()] * STATE_GAIN;
    //    current += vs[inpt[i].lrx()][inpt[i].lry()] * STATE_GAIN;
    ////    if (current>1.0f) current *= STATE_BOUNDS_NL;
    ////    if (current<-1.0f) current *= -STATE_BOUNDS_NL;
    //    current = delay.tick(logmap(current,R));
    //    vs[inpt[i].llx()][inpt[i].lly()] += inpt[i].llf() * current;
    //    vs[inpt[i].ulx()][inpt[i].uly()] += inpt[i].ulf() * current;
    //    vs[inpt[i].urx()][inpt[i].ury()] += inpt[i].urf() * current;
    //    vs[inpt[i].lrx()][inpt[i].lry()] += inpt[i].lrf() * current;

    for(int i = 0; i < nX; i++) // sum input vertically
    {
        out[ i ] = 0.0;
        for(int j = 0; j < nY; j++)
            out[ i ] += in[ i ] [ j ];
    }

    for(int i = 0; i < N_OUTCHANS; i++)
    {
        //        int k = (i<N_INPTS) ? i : N_INPTS-1;
        lastFrame_[i] = 0.0;
        if(i < MAX_OUTCHANS) {
            for (int j = 0; j < outpt[i].sides; j++) {
                //            lastFrame_[i] += in[ inpt[k].rx[j] ] [ inpt[k].ry[j] ] * inputMixScl;
                lastFrame_[i] += vs[ outpt[i].rx[j] ] [ outpt[i].ry[j] ] * meshMixScl;
            }
            float tmp = 0.0f;
            for (int j = 0; j < nX; j++) {
                tmp += (out[ j ] * prox[ j ] [ i ]);
            }
            lastFrame_[i] += tmp * inputMixScl;
            lastFrame_[i] = dcBlocker[i]->tick(lastFrame_[i]);
            lastFrame_[i] = onePoleOutput[i]->tick(lastFrame_[i]);
        }
    }

#define xpTick(x,y) xpz [x+1] [y] = \
    xp [x+1] [y]
#define xpScat(x,y) xp [x+1] [y] = \
    vs[x][y] - xm [x+1] [y]

#define ypTick(x,y) ypz [x] [y+1] = \
    yp [x] [y+1]
#define ypScat(x,y) yp [x] [y+1] = \
    vs[x][y] - ym [x] [y+1]

#define xmTick(x,y) xmz [x] [y] = \
    xm [x] [y]
#define xmScat(x,y) xm [x] [y] = \
    vs[x][y] - xpz [x] [y]

#define ymTick(x,y) ymz [x] [y] = \
    ym [x] [y]
#define ymScat(x,y) ym [x] [y] = \
    vs[x][y] - ypz [x] [y]

    for (int x=0; x<nX-1; x++) {
        for (int y=0; y<nY-1; y++) {
            xpTick(x,y);
            xpScat(x,y);
            ypTick(x,y);
            ypScat(x,y);
            xmTick(x,y);
            xmScat(x,y);
            ymTick(x,y);
            ymScat(x,y);
        }
    }

    //    for(int i = 0; i < nOut; i++)
    //    {
    //        lastFrame_[i] = input;
    //    }

#ifdef WORKERS
    //    qDebug() << "calling tick";
    for (int i = 0; i < WORKERS; i++)
        workerThreads[i]->tick();
    joinWorkerThreads();
    //    qDebug() << "after join";
#else
    leftEdge();
    rightEdge();
    bottomEdge();
    topEdge();
#endif
    return lastFrame_;
}

void GenMesh::joinWorkerThreads() {
#ifdef WORKERS
    bool join = true;
    while (join) {
        join = false;
        for (int i = 0; i < WORKERS; i++) {
            if (!workerThreads[i]->isWaiting())
                join = true;
        }
        //        qDebug() << join;
        QThread::yieldCurrentThread();
    }
#else
#endif
}

#define xpEdgeFilt(y) xp [0] [y] = \
    filterY_[y].tick(xmz [0] [y])
#define xpEdgeTick(y) xpz [0] [y] = \
    xp [0] [y]

#define xpEdgeFilt2(y) xm [nX-1] [y] = \
    filterY2_[y].tick(xpz [nX-1] [y])
#define xpEdgeTick2(y) xmz [nX-1] [y] = \
    xm [nX-1] [y]

#define xmEdgeRefl(y) xm [nX-1] [y] = \
    xpz [nX-1] [y]
#define xmEdgeReflInverting(y) xm [nX-1] [y] = \
    -xpz [nX-1] [y]
#define xmEdgeOut(y) output[y] = \
    xpz [nX-1] [y]

#define ypEdgeFilt(x) yp [x] [0] = \
    filterX_[x].tick(ymz [x] [0])
#define ypEdgeTick(x) ypz [x] [0] = \
    yp [x] [0]

#define ypEdgeFilt2(x) ym [x] [nY-1] = \
    filterX2_[x].tick(ypz [x] [nY-1])
#define ypEdgeTick2(x) ymz [x] [nY-1] = \
    ym [x] [nY-1]

#define ymEdgeRefl(x) ym [x] [nY-1] = \
    ypz [x] [nY-1]
#define ymEdgeReflInverting(x) ym [x] [nY-1] = \
    -ypz [x] [nY-1]
#define ymEdgeIn(x) ym [x] [nY-1] = \
    input[x]
#define ymEdgeOut(x) input[x] = \
    ypz [x] [nY-1]

float GenMesh::leftEdgeX(int beg, int end, float in)
{
    float out = 0.0;

    //    out = filterY_[0].tick(in); // original filters
    //    for (int j=0; j<4; j++)
    //        out = biquadY_[0][j].tick(out);

    // onepole only
    //        fRec0[0][0] = (0.0399999991f * fRec0[1][0]) + float(in);
    //        out = float((1.0f * fRec0[0][0]));
    //        fRec0[1][0] = fRec0[0][0];

    //    out = edgeFilterY_[0].tick(in); // faust filter

    return out;

}

void GenMesh::leftEdge(int beg, int end)
{
#ifdef WORKERS
    for (int i=beg; i<end; i++) {
        //        qDebug() << beg << end << i << "leftEdge";
#else
    for (int i=0; i<nY; i++) {
#endif
        xpEdgeFilt(i);
#ifdef APF
        //        qDebug() << stretchOff << stretchSclNeg << stretchSclPos;
        probe[0][i] = setStretchbiquadY(xp[0][i], stretchOff, stretchSclNeg, stretchSclPos, i, ORDER);
        for (int j=0; j<ORDER; j++)
            xp[0][i] = biquadY_[i][j].tick(xp[0][i]);
#endif
        xpEdgeTick(i);
    }
}

void GenMesh::rightEdge(int beg, int end)
{
#ifdef WORKERS
    for (int i=beg; i<end; i++) {
        //        qDebug() << beg << end << i << "rightEdge";
#else
    for (int i=0; i<nY; i++) {
#endif
#ifdef FILTERINGALLX
        xpEdgeFilt2(i);
        xpEdgeTick2(i);
#else
#ifndef INVERTINGX
        xmEdgeRefl(i);
#else
        xmEdgeReflInverting(i);
#endif
#endif
    }
}

void GenMesh::bottomEdge(int beg, int end)
{
#ifdef WORKERS
    for (int i=beg; i<end; i++) {
        //        qDebug() << beg << end << i << "bottomEdge";
#else
    for (int i=0; i<nX; i++) {
#endif
        ypEdgeFilt(i);
#ifdef APF
        setStretchbiquadX(yp[i][0], stretchOff, stretchSclNeg, stretchSclPos, i, ORDER);
        for (int j=0; j<ORDER; j++)
            yp[i][0] = biquadX_[i][j].tick(yp[i][0]);
#endif
        ypEdgeTick(i);
    }
}

void GenMesh::topEdge(int beg, int end)
{
#ifdef WORKERS
    for (int i=beg; i<end; i++)
#else
    for (int i=0; i<nX; i++) {
#endif
#ifdef FILTERINGALLY
        ypEdgeFilt2(i);
        ypEdgeTick2(i);
#else
#ifndef INVERTINGY
        ymEdgeRefl(i);
#else
        ymEdgeReflInverting(i);
#endif
#endif
    }
}

GenMesh :: ~GenMesh()
{
#ifdef WORKERS
    for (int i = 0; i < WORKERS; i++)
        workerThreads[i]->done = true;
#else
#endif
}

void GenMesh :: noteOn( StkFloat frequency, StkFloat amplitude )
{
}

void GenMesh :: noteOff( StkFloat amplitude )
{
}

void GenMesh :: setNX( unsigned short lenX )
{
    if ( lenX < 2 ) {
        oStream_ << "Mesh2Dcc::setNX(" << lenX << "): Minimum length is 2!";
        handleError( StkError::WARNING ); return;
    }

    nX = lenX;
}

void GenMesh :: setNY( unsigned short lenY )
{
    if ( lenY < 2 ) {
        oStream_ << "Mesh2Dcc::setNY(" << lenY << "): Minimum length is 2!";
        handleError( StkError::WARNING ); return;
    }

    nY = lenY;
}

float** GenMesh :: floatVect( int x, int y)
{
    float** v;
    v = new float* [x];
    for(int i = 0; i < x; i++)
    {
        v[i] = new float [y]();
    }
    return v;
}

float GenMesh :: setStretch( float in, float off,
                             float sclNeg, float sclPos )
{
    float stretch =  off + (( (in<0.0) ? sclNeg : sclPos ) * in);
    //    stretch = (fabs(stretch) > 1.0) ? (stretch * 0.09) : stretch;
    //    if (fabs(stretch) > 1.0) qDebug() << "hi";
    stretch = (stretch > 1.0) ? 1.0f : stretch;
    stretch = (stretch < 0.0) ? 0.0f : stretch;
    return stretch;
}

double GenMesh :: setStretchbiquadY( float in, float off,
                                     float sclNeg, float sclPos, int i, int o )
{
    int y = i;
    float stretch = setStretch(in, off, sclNeg, sclPos);
    StkFloat coef1 = stretch * stretch;
    StkFloat coef2 =  -2.0 * stretch;
    double tmp = 0.0;

    for ( int i=0; i<o; i++ )	{
        biquadY_[y][i].setA2( coef1 );
        biquadY_[y][i].setB0( coef1 );

        tmp = coef2 * cos(fscl * freq[i]);
        biquadY_[y][i].setA1( tmp );
        biquadY_[y][i].setB1( tmp );
    }
    return stretch;
}

void GenMesh :: setStretchbiquadX( float in, float off,
                                   float sclNeg, float sclPos, int i, int o )
{
    int x = i;
    float stretch = setStretch(in, off, sclNeg, sclPos);
    StkFloat coef1 = stretch * stretch;
    StkFloat coef2 =  -2.0 * stretch;

    for ( int i=0; i<o; i++ )	{
        biquadX_[x][i].setA2( coef1 );
        biquadX_[x][i].setB0( coef1 );

        double tmp = coef2 * cos(fscl * freq[i]);
        biquadX_[x][i].setA1( tmp );
        biquadX_[x][i].setB1( tmp );
    }
}
