#ifndef JACKAUDIOINTERFACE_H
#define JACKAUDIOINTERFACE_H

#include <QThread>
#include <iostream>
#include <functional> //for mem_fun_ref
#include <jack/jack.h>
#include "genmesh.h"
#include "striker.h"
using namespace stk;

class JackAudioInterface : public QThread
{
    Q_OBJECT
public:
    explicit JackAudioInterface(int I, int O);
    /// \brief The class destructor
    virtual ~JackAudioInterface();
    void run();
    void setMesh(GenMesh *m);
    void setCollision(double x, double y);
    /// \brief Setup the client
    void setupClient();
    jack_client_t* mClient; ///< Jack Client
    static void jackShutdown(void*);
    void setStriker(Striker *s);
private:
    int nI;
    int nO;
    int nXc;
    int nYc;
    GenMesh *mMesh;
    GenMesh *mCollision;
    int processCallback (jack_nframes_t nframes, void *arg);
    static int processCallbackWrapper(jack_nframes_t nFrames, void *arg);
    QVarLengthArray<jack_port_t*> mInPorts; ///< Vector of Input Ports (Channels)
    QVarLengthArray<jack_port_t*> mOutPorts; ///< Vector of Output Ports (Channels)
    typedef float sample_t;
    QVarLengthArray<sample_t*> mInBuffer; ///< Vector of Input buffers/channel read from JACK
    QVarLengthArray<sample_t*> mOutBuffer; ///< Vector of Output buffer/channel to write to JACK
    Striker * mStriker;
    double mPosX;
    double mPosY;

signals:
};

#endif // JACKAUDIOINTERFACE_H
