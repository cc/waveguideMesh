#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "globals.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    setMouseTracking(true); // E.g. set in your constructor of your widget.
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    socket = new QTcpSocket(this);
    connect( socket,SIGNAL(connected()),this,SLOT(connected()));
    connect( socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    connect( socket,SIGNAL(error(QAbstractSocket::SocketError)),
             this,SLOT(error(QAbstractSocket::SocketError)));
    connect( socket,SIGNAL(hostFound()),this,SLOT(hostFound()));
//    connect( socket,SIGNAL(bytesWritten(qint64)),this,SLOT(bytesWritten(qint64)));
    connect( socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    socket->connectToHost(ui->hostLineEdit->text(),SSR_PORT);
    // setStriker in main
    connect( mStriker,SIGNAL(strikeMove(double, double)),this,SLOT(wiggle(double, double)));
    autoMove(0.0, 1.5);
}

void MainWindow::setStriker (Striker *s) {
    mStriker=s;
}

void MainWindow::wiggle(double x, double y)
{
    autoMove(x, y);
}

void MainWindow::on_f1_clicked()
{
    sa.setPhasors(1);
}

void MainWindow::on_f2_clicked()
{
    sa.setPhasors(2);
}

void MainWindow::on_f3_clicked()
{
    sa.setPhasors(3);
}

void MainWindow::on_f4_clicked()
{
    sa.setPhasors(4);
}

void MainWindow::on_f5_clicked()
{
    sa.setPhasors(5);
}

void MainWindow::on_f6_clicked()
{
    sa.setPhasors(6);
}
/////////////////

void MainWindow::on_f11_clicked()
{
    sa.setPhasors(11);
}

void MainWindow::on_f12_clicked()
{
    sa.setPhasors(12);
}

void MainWindow::on_f13_clicked()
{
    sa.setPhasors(13);
}

void MainWindow::on_f14_clicked()
{
    sa.setPhasors(14);
}

void MainWindow::on_f15_clicked()
{
    sa.setPhasors(15);
}

void MainWindow::on_f16_clicked()
{
    sa.setPhasors(16);
}

void MainWindow::on_fixedArrayCheckBox_stateChanged()
{
    sa.toggleFixedArray();
}

void MainWindow::on_synthCheckBox_stateChanged()
{
    mStriker->toggleSynth();
}

void MainWindow::mouseMoveEvent(QMouseEvent *event) {
//    if(event->button()==Qt::NoButton) { // a move event
//        pt = event->pos();
//        double dX = lastPt.x() - pt.x();
//        double dY = lastPt.y() - pt.y();
//        //    qDebug() << dX << dY;
//        sa.moveX(dX*0.01);
//        sa.moveY(dY*0.01);
//    } else {
//        sa.setX(.1);
//        sa.setY(.1);
//    }
//    for (int i=0; i<31; i++) {
//        QString request("<request><source id='"+QString::number(i+1)+
//                        "'><position x='"+QString::number(sa.getX(i))+
//                        "' y='"+QString::number(sa.getY(i))+
//                        "'/></source></request>" );
////        qDebug() << request;
//        QByteArray ba;
//        ba.append(request);
//        ba.append('\0');
//        socket->write(ba);
//    }
//    lastPt = pt;
}

void MainWindow::autoMove(double x, double y) {
    sa.setX(x);
    sa.setY(y);
    for (int i=0; i<48; i++) {
        QString request("<request><source id='"+QString::number(i+1)+
                        "'><position x='"+QString::number(sa.getX(i))+
                        "' y='"+QString::number(sa.getY(i))+
                        "'/></source></request>" );
//        qDebug() << request;
        QByteArray ba;
        ba.append(request);
        ba.append('\0');
        socket->write(ba);
    }
}

void MainWindow::readyRead()
{
    quint16 bytes =  socket->bytesAvailable();
    //    ui->outputTextEdit->append("----------------------------------------");
    //    ui->outputTextEdit->append(QString::number(bytes)+ " for you to read");
    //    ui->outputTextEdit->append("----------------------------------------");
    //    QByteArray ba= socket->readAll();
    //    QString dataString (ba);
    //    QString pos("<source id='5'><position");
    //    if (dataString.contains(pos)) {
    //        ui->outputTextEdit->append(dataString);
    //        QString request0("<request><source id='2'><position x='0.0' y='0.0'/></source></request>" );
    //        QByteArray ba;
    //        ba.append(request0);
    //        ba.append('\0');
    //        socket->write(ba);
    //    }
}


void MainWindow::connected()
{
    ui->outputTextEdit->append("Connected to server");
}

void MainWindow::disconnected()
{
    ui->outputTextEdit->append("Disconnected from server");
}

void MainWindow::error(QAbstractSocket::SocketError socketError)
{
    QString errorStr=socket->errorString();
    ui->outputTextEdit->append("An error occured :"+ errorStr);
}

void MainWindow::hostFound()
{
    ui->outputTextEdit->append("Host found");
}

void MainWindow::bytesWritten(qint64 bytes)
{
    QString outString = QString::number(bytes) + " bytes writen.";
    ui->outputTextEdit->append(outString);

}

void MainWindow::on_disconnectButton_clicked()
{
    socket->disconnectFromHost();
}
