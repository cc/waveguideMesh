#ifndef GLOBALS_H
#define GLOBALS_H
#include <math.h>
#define SSR_PORT 6000
static int FS = 48000;
static double cair = 343.0;
static double cbrass = 0.0508 / 0.00005; // from mic & LDV observations
//static double cbrass = 1333.5; // m/s from article 1333.5
static double tsamp = (1.0/(double)FS);
static double mPerSampAir = tsamp * cair;
static double mPerSampBrass = tsamp * cbrass;
/////////////// plate
//#define X 0.40 // small plate
//#define Y 0.32
#define X 0.685 // article plate
#define Y 0.178
//#define X 0.37 // ness plate
//#define Y 0.31
//static double Y = X * 4.3333;
/////////////// mesh
static int nX = (int) round(X/mPerSampBrass);
static int nY = (int) round(Y/mPerSampBrass);
/////////////// outputs
// #define FIXED_SPACING 0.0372f // soundbender
// #define FIXED_SPACING 0.0508f // mic
//static double X = FIXED_SPACING * 11; // soundbender 0.4096
static float HORIZ_MIDLINE = nY/2.0f; // horizontal row
/////////////// inputs
static int N_INPTS = 1;
/////////////// damping points
static int N_DAMPS = 2;
#define CONTACT_DAMPING 0.99999f // -0.00001f // 0.99f
/////////////// gains
#define EXC_GAIN 1.0f // article 0.7, 60 sec 1.5, self-excited 100.0
static float meshMixScl = 0.5f;
static float inputMixScl = 0.05f;
//#define IN_GAIN 2.0f // article 1.0, 60 sec 0.5
//#define OUT_GAIN 1.0f // article 0.01, 60 sec 0.03, self-excited 0.05
/////////////// filters
static float pole = 0.05;
static float decay = 0.999; //999;
#define ORDER 4
#define MAGIC 0.01

#define OUT_FILE "sharpForceHammer.wav"
#define OUT_FILE "test.wav"
static int N_OUTCHANS = 2; // 12; // 2; // article, soundbender or can be nX

#define APF
// ap0
//#undef APF

// ap1
#define stretchOff 0.0f
#define stretchSclNeg 0.0f
#define stretchSclPos 0.0f

// ap2
//#define stretchOff 0.73f
//#define stretchSclNeg 0.0f
//#define stretchSclPos 0.0f

// ap3
//#define stretchSclNeg 0.5f
//#define stretchSclPos 0.5f

// ap4
//#define stretchSclNeg 0.04f
//#define stretchSclPos 0.55f

// final
#define EXC_GAIN 2.5f // article 0.7, 60 sec 1.5, self-excited 100.0
#define stretchOff 0.75f
#define stretchSclNeg 0.05f
#define stretchSclPos 0.01f


// #define STKMESH

//#define FILTERINGALLX
//#define FILTERINGALLY
//#define INVERTINGX
//#define INVERTINGY
//#define VERBOSE

#endif // GLOBALS_H
