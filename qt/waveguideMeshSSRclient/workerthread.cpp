#include "workerthread.h"
#include "genmesh.h"

WorkerThread::WorkerThread(QObject *mesh, int id, int n, int x, int y) :
    myID(id), nWorkers(n), nX(x), nY(y)
{
    this->mesh = mesh;
    done = false;
    go = false;
    setWaiting(true);

    int xChunk = nX/nWorkers;
    xBeg = myID * xChunk;
    xEnd = (xBeg + xChunk);
//    qDebug() << xBeg << xEnd;
    if (myID == n-1) xEnd = nX;
    qDebug() << xEnd;

    int yChunk = nY/nWorkers;
    yBeg = myID * yChunk;
    yEnd = (yBeg + yChunk);
//    qDebug() << yBeg << yEnd;
    if (myID == n-1) yEnd = nY;
    qDebug() << yEnd;
}

void WorkerThread::tick()
{
    QMutexLocker locker(&mMutex); // fixes timing race in polling across threads
    setGo(true);
    setWaiting(false);
}

void WorkerThread::edges()
{
    QMutexLocker locker(&mMutex); // fixes timing race in polling across threads
    GenMesh *tmp = (GenMesh *)mesh;
    tmp->leftEdge(yBeg, yEnd);
    tmp->rightEdge(yBeg, yEnd);
    tmp->bottomEdge(xBeg, xEnd);
    tmp->topEdge(xBeg, xEnd);
    setGo(false);
    setWaiting(true);
//    qDebug() << "done" << myID;
}

