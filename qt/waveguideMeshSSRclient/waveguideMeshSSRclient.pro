#-------------------------------------------------
#
# Project created by QtCreator 2016-06-27T19:50:27
#
#-------------------------------------------------

QT       += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = waveguideMeshSSRclient
TEMPLATE = app

DEFINES += __UNIX_JACK__
QMAKE_CXXFLAGS += -O3 -mfpmath=sse -msse -msse2 -msse3 -ffast-math

SOURCES += main.cpp\
        mainwindow.cpp \
    genmesh.cpp \
    fileio.cpp \
    wavfileio.cpp \
    jackaudiointerface.cpp \
    sourcearray.cpp \
    striker.cpp \
    pt.cpp \
    workerthread.cpp

HEADERS  += mainwindow.h \
    genmesh.h \
    fileio.h \
    globals.h \
    wavfileio.h \
    jackaudiointerface.h \
    sourcearray.h \
    striker.h \
    pt.h \
    workerthread.h

FORMS    += mainwindow.ui

LIBS += -lpthread -ljack

# stk latest from
# https://ccrma.stanford.edu/software/stk/download.html
# autoconf
# ./configure  --with-jack
# make, make install
# sudo ln /usr/local/lib/libstk-4.6.0.so /usr/lib64/
# includes preceded with stk/ should be ok as well as lib
LIBS += -lstk
# LIBS += -lrtaudio
