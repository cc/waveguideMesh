#include "pt.h"
#include <math.h>
#include <QDebug>
#include "globals.h"

Pt::Pt()
{
    sides = MATRIX_SIZE;
    mPiOverTwo = 3.14159265359f * 0.5f;
}

void Pt::rectangleAround(float x, float y)
{
    // bounding indices
    int mlx; // left of x
    int mrx; // right of x
    int mly; // lower of y (below)
    int muy; // upper of y (above)
    // interpolated fractions
    float mlxf; // left frac
    float mrxf; // right frac
    float mlyf; // lower frac
    float muyf; // upper frac
    // corner fractions clockwise
    float mllf; // lower left corner frac
    float mulf; // upper left corner frac
    float murf; // upper right corner frac
    float mlrf; // lower right corner frac
    mx=x;
    my=y;
    mlx = int(floor(mx));
    mrx = mlx+1;
    mly = int(floor(my));
    muy = mly+1;

    rx[0] = mlx;
    ry[0] = mly;

    rx[1] = mlx;
    ry[1] = muy;

    rx[2] = mrx;
    ry[2] = muy;

    rx[3] = mrx;
    ry[3] = mly;

    mlxf = x-float(mlx);
    mrxf = 1.0f - mlxf;
    mlyf = y-float(mly);
    muyf = 1.0f - mlyf;
    //  qDebug() << mlxf << mlyf;

    s[0] = mllf = mlxf * mlyf;
    s[1] = mulf = mlxf * muyf;
    s[2] = murf = mrxf * muyf;
    s[3] = mlrf = mrxf * mlyf;

#ifdef VERBOSE
    float sum = mllf+mulf+murf+mlrf;
    if(sum!=1.0f) qWarning() << "sum not 1.0" << sum-1.0f;
#endif
}

float Pt::fullSwingScaledToDimension(float v, int r) {
    float rOver2 = (float)r / 2.0f;
//    qDebug() << v << rOver2;
    return rOver2 + (v*rOver2);

}

void Pt::xyNormalized(float x, float y) // range -1.0 to 1.0
{
//#define halfSwing(v) 0.5f * (1.0f + v)
//    mxPan=(float)halfSwing(x) * mPiOverTwo;
//    myPan=(float)halfSwing(y) * mPiOverTwo;

    mx=fullSwingScaledToDimension(x,nX-1);
    my=fullSwingScaledToDimension(y,nY-1);
    rectangleAround(mx,my); // set mMatrix fractions
}

void Pt::xyAbsolute(float x, float y) // node units
{
    mx=x;
    my=y;
    rectangleAround(mx,my);
}

void Pt::xyNormalizedMaxX(float x, float y, int max) // range -1.0 to 1.0
{
//#define halfSwing(v) 0.5f * (1.0f + v)
//    mxPan=(float)halfSwing(x) * mPiOverTwo;
//    myPan=(float)halfSwing(y) * mPiOverTwo;

    mx=fullSwingScaledToDimension(x,max-1);
    my=fullSwingScaledToDimension(y,nY-1);
    rectangleAround(mx,my); // set mMatrix fractions
}
