#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMouseEvent>
#include <QTcpSocket>
#include <QMainWindow>
#include "sourcearray.h"
#include "striker.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void mouseMoveEvent(QMouseEvent *event);
    void autoMove(double x, double y);
    void setStriker (Striker *s);

private slots:
    void on_connectButton_clicked();
    void on_f1_clicked();
    void on_f2_clicked();
    void on_f3_clicked();
    void on_f4_clicked();
    void on_f5_clicked();
    void on_f6_clicked();

    void on_f11_clicked();
    void on_f12_clicked();
    void on_f13_clicked();
    void on_f14_clicked();
    void on_f15_clicked();
    void on_f16_clicked();

    void on_fixedArrayCheckBox_stateChanged();
    void on_synthCheckBox_stateChanged();

    void connected();
    void disconnected();
    void error(QAbstractSocket::SocketError socketError);
    void hostFound();
    void bytesWritten(qint64 bytes);
    void readyRead();
    void wiggle(double x, double y);

    void on_disconnectButton_clicked();

private:
    Ui::MainWindow *ui;
    QTcpSocket * socket;
    QPoint pt;
    QPoint lastPt;
    double x[17];
    double y;
    SourceArray sa;
    Striker * mStriker;
};

#endif // MAINWINDOW_H
