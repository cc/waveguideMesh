#ifndef STRIKER_H
#define STRIKER_H
#include <QThread>


class Striker : public QThread
{
    Q_OBJECT
public:
    Striker();
    void run();
    double x;
    double y;
    void toggleSynth();
    bool synth;
private:
signals:
    void strikeMove(double x, double y);
    void strikeAudio(double v);
    void strikePos(double x, double y);
};

#endif // STRIKER_H
