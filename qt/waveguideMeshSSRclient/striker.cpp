#include "striker.h"
#include <QDebug>
#include <math.h>
Striker::Striker() :
    synth(true)
{
    x = 0;
    y = 0;
}

void Striker::toggleSynth()
{
    synth = !synth;
}

#define CHUCK 1
#ifdef CHUCK
void Striker::run()
{
    while (true) {
        emit strikeMove(0,0.5); // (x/0.2+0.2,y/2+1.0);
        msleep(100);
    }
}
#else
void Striker::run()
{
        int nChans = 12;
        double xincSpacing = 0.0145;
        double xleft = (nChans/2) * -xincSpacing;
        double x = xleft;
        double y = 1.2;
        double posX = 0.0;
        double posXinc = 0.74;
        double posY = -1.0;
        double posYinc = 0.53;
        double z = 0.01;
        double bufSamp = (256/48000.0);
        double bufMs = bufSamp * 1000;
        while (true)
        {
            emit strikeMove(2.5*posX,y+fabs(2.5*posY));
            posX+=posXinc;
            posY+=posYinc;
            if(posX>1.0)posX-=2.0;
            if(posY>1.0)posY-=2.0;
            msleep(1500);
//            emit strikePos(posX,posY);
            z = 0.25;
            emit strikeAudio(z);
            msleep(bufMs);
            z = 0.0;
            emit strikeAudio(z);
            msleep(bufMs);
            z = -0.25;
            emit strikeAudio(z);
            msleep(bufMs);
            z = 0.0;
            emit strikeAudio(z);
            ///////////////////////////////////////////
            //        msleep(150);
            double xinc = 0.01;
            double yinc = 0.03;
            int cnt =10;
            for (int i = 0; i < cnt; i++) {
                double frac = (double)(cnt-i)/(double)cnt;
                msleep(10);
                emit strikeMove(2.5*posX-(frac*xinc),y+fabs(2.5*posY)-(frac*yinc));
//                msleep(8);
//                emit strikeMove(2.5*posX,y+fabs(2.5*posY));
            }
        }
}
#endif
