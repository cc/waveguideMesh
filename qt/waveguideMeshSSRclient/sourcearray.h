#ifndef SOURCEARRAY_H
#define SOURCEARRAY_H

#include <QObject>

#define nChans 48

class SourceArray
{
public:
    SourceArray();
    void setX(double x);
    void setY(double y);
    void moveX(double dX);
    void moveY(double dY);
    double getX(int i);
    double getY(int i);
    void setPhasors(int which);
    void toggleFixedArray();
private:
    double xOff;
    double xInc;
    double x[nChans];
    double yOff;
    double yInc;
    double y[nChans];
    void update(bool fixedArray = 0);
    double phasor[nChans];
    double inc[nChans];
    bool fixedArray;
};

#endif // SOURCEARRAY_H
