#include "wavfileio.h"
#include "stk/FileWvOut.h"
#include "stk/FileWvIn.h"
#include "globals.h"

#ifdef STKMESH
#include "stk/Mesh2D.h"
#endif
using namespace stk;

WavFileIO::WavFileIO(int I, int O) : nI(I),nO(O)
{
}

void WavFileIO::start()
{
    qDebug() << "read data / write data mode";
    QString tmpInDir("../../wav/");
    QString tmpInName;
    QString posInName; // stereo x/y position
    QString tmpOutDir("../../wav/");
    QString tmpOutName;
    //    tmpInName = "6minR.wav";
    tmpInName = "strikeTrain.wav";
    tmpInName = "iterTrain.wav";
    tmpInName = "iterTrainSimulateForceHammer.wav";
//    tmpInName = "strikeSaw.wav";
//            tmpInName = "strike.wav";
//            tmpInName = "sharpForceHammerCleanForSonogram.wav"; // 0.1 sec, strike, 0.9 sec
    //            tmpInName = "sharpForceHammerClean60sec.wav"; // 60 strikes 60 sec
//        tmpInName = "sharpForceHammerRing.wav"; // 1 strike 7 sec
    //    tmpInName = "sharpForceHammer.wav"; // 1 sec, not clean
//            tmpInName = "impulse.wav"; // from impulse.ck 1 sec, impulse, 1 sec
//        tmpInName = "strikeTrain.wav"; // 5 strikes 5 sec
    //    tmpInName = "strikes.wav";
    //    tmpInName = "yoshiFiles/1-treeRingPlucks.wav";
    //    tmpInName = "yoshiFiles/2-scrunchRhythm.wav";
    //    tmpInName = "yoshiFiles/3-slowBubbles.wav";
    //    tmpInName = "yoshiFiles/4-stove.wav";
    //    tmpInName = "yoshiFiles/5-fastBubbles.wav";
    //    tmpInName = "yoshiFiles/6-earthPlucks.wav";
//        tmpInName = "cellettoForSamantha/spiccato.wav";
//    posInName = "pos.wav";
    posInName = "iterPos.wav";
//    posInName = "posSaw.wav";
    //    tmpOutName = "meshStretchTest.wav";
    tmpOutName = OUT_FILE;
    StkFrames outFrame( 1, nO );

#ifdef STKMESH
    qDebug() << Stk::sampleRate();
    qDebug() << "output hardwired to" << nX-2 << nY-2;
    Mesh2D stkMesh(nX, nY);
#endif

    FileWvOut wavOut;
    QString* my_oFilename;
    my_oFilename = new QString(tmpOutDir + tmpOutName);
    std::string tmp = my_oFilename->toStdString();
    wavOut.openFile( tmp,
                     nO, FileWrite::FILE_WAV, Stk::STK_SINT16 );
    qDebug() << nO << "output channels";

    FileWvIn wavIn(1024,1024); // read incrementally so that normalization is ok
    QString* my_iFilename;
    my_iFilename = new QString(tmpInDir + tmpInName);
    tmp = my_iFilename->toStdString();
    wavIn.openFile( tmp ); // default format and correct normalization
    FileWvIn posIn(1024,1024); // read incrementally so that normalization is ok
    my_iFilename = new QString(tmpInDir + posInName);
    tmp = my_iFilename->toStdString();
    posIn.openFile( tmp ); // default format and correct normalization

    posIn.tick();
    StkFrames xy = posIn.lastFrame();
//    xy[0] = 0.0;     xy[1] = 0.0;   // default mid in mesh constructor, agrees with 1 output
        xy[0] = -1.0;   xy[1] = -1.0; // lower left for Mesh2D test
    //    xy[0] = -0.55;   xy[1] = 0.0; // static pos for article

        FileIO probeFile;
        probeFile.open(QString("/tmp/xxx.dat"));
    int i = 0;
//    float talkExamples = 14.0; // 2.9;
        while ((!wavIn.isFinished())) // && (!posIn.isFinished()))
//    for(int i = 0; i < talkExamples*48000; i++)
    {
                i++;

        float input = wavIn.tick();

        posIn.tick(xy);

#ifdef STKMESH
        stkMesh.setDecay(decay);

#define halfSwing(v) 0.5f * (1.0f + v)
        float tmpX = halfSwing(xy[0]);
        float tmpY = halfSwing(xy[1]);
        stkMesh.setInputPosition(tmpX,tmpY);
#ifdef VERBOSE
        qDebug() << (unsigned short) (tmpX * (nX - 1))  << (unsigned short) (tmpY * (nY - 1));
#endif
        stkMesh.noteOn(0.0,input);
        stkMesh.tick(outFrame);
        outFrame[0] = inputMixScl*input + meshMixScl*outFrame[0];
#else
        mesh->inpt[0].xyNormalized( xy[0], xy[1] );
//        mesh->inpt[0].xyNormalizedMaxX( xy[0], xy[1], mesh->MAX_OUTCHANS );
#ifdef VERBOSE
        qDebug() << mesh->inpt->llx()  << mesh->inpt->lly();
#endif
        outFrame = mesh->inputTick(input);

//        double tmp = mesh->probe[ 0 ] [ nY/2 ];
//        if (fabs(tmp) > 100.0) break;
//        probeFile.writeDouble(tmp);

#endif
#ifdef MONO
        for (int i = 1; i < nO; i++) outFrame[i] = 0.0;
#endif
        wavOut.tick(outFrame);
        if (!(i%(1*48000)))
            qDebug() << i/48000;
    }
    qDebug() << "done";
    probeFile.close();
}
//        wavOut.tick(mesh->inputTickX(wavIn.tick())); // edge filter impulse response
