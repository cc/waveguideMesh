#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include <QLoggingCategory>
#include <QTimer>
#include <sched.h>  // sched_setaffinity
#include "wavfileio.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    fprintf(stderr, "%s\n", localMsg.constData());
    fflush(stderr);
}

int main(int argc, char *argv[])
{
    int cpuAffinity = argc > 1 ? atoi(argv[1]) : 0; // force to CPU 0 by default

    if (cpuAffinity > -1)
    {
        cpu_set_t mask;
        int status;

        CPU_ZERO(&mask);
        CPU_SET(cpuAffinity, &mask);
        status = sched_setaffinity(0, sizeof(mask), &mask);
        if (status != 0)
        {
            perror("sched_setaffinity");
        }
    }
    QApplication a(argc, argv);
    QLoggingCategory::setFilterRules(QStringLiteral("*.debug=true"));
    qInstallMessageHandler(myMessageOutput);
    MainWindow w;
//    w.show();

    int x = 25; // 2 per mic spacing of 5.07cm along 67.5
    int y = 6;
    qDebug() << "plate = " << x << "by" << y;
#define N_OUTCHANS 12 // settings
    WavFileIO jack(1, N_OUTCHANS, x, y ); // nI(I),nO(O),nX(X),nY(Y)
    jack.setMesh(); // new stk::GenMesh(nX,nY,nO,FS)
    jack.start();
//    QTimer::singleShot(1500, &a,  SLOT(quit()));

//      a.exec();

    return 1;
}
