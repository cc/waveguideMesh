#ifndef FILEIO_H
#define FILEIO_H

#include <QObject>
#include <QFile>
#include <QString>
#include <QDebug>
#include <QTextStream>

class FileIO : public QObject
{
public:
    FileIO();
    void open(QString path);
    void writeLine(QString line);
    void close();
private:
    QFile* file;
public slots:
    void writeDouble(double val);
    void writeNextColumn(double val);
    void endLine();
};

#endif // FILEIO_H
