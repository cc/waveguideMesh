#include "wavfileio.h"
#include "stk/FileWvOut.h"
#include "stk/FileWvIn.h"

//#define FS 12000 rings actively

//#define FS 24000 // octave down, compressed space,
// but scaling to a cluster of 12ch in mid

// #define FS 96000 // octave up, expanded space,
// but reducing to first 6ch fits nicely

#define FS (StkFloat) 48000.0

WavFileIO::WavFileIO(int I, int O, int X, int Y) : nI(I),nO(O),nX(X),nY(Y)
{

}

void WavFileIO::setMesh()
{
    mesh = new GenMesh(nX,nY,nO,FS);
//    mesh->mapOutputPositions(-1.0,-1.0,1.0,1.0); // diagonal linear taps, quantized

    mesh->mapOutputPositions(-1.1,0.0,1.1,0.0); // cross section, halfway up, force X nearer to edge for article

//    mesh->mapOutputPositions(-1.0,0.0,1.0,0.0); // cross section, halfway up
//    mesh->mapOutputPositions(-0.5,0.0,0.5,0.0); // tap only half of the object

    // horiz array adjacent cells starting from midpoint
////    mesh->mapOutputPositions(0.0,0.0,0.0,0.0, true);

    mesh->setStretch(0.5); // 0.0 = no effect, 1.0 = full // 59x42 exs 0.99
}

void WavFileIO::start()
{
    qDebug() << "read data / write data mode";
    QString tmpInDir("../../wav/");
    QString tmpInName;
    QString posInName; // stereo x/y position
    QString tmpOutDir("../../wav/");
    QString tmpOutName;
//    tmpInName = "sharpForceHammerX6.wav"; // 6 strikes 6 sec
    tmpInName = "sharpForceHammer.wav"; // 1 strike 1 sec
//    tmpInName = "strikeTrain.wav"; // 5 strikes 5 sec
//    tmpInName = "strikes.wav";
//    tmpInName = "celletto.wav";
    posInName = "dc.wav";
//    posInName = "strikePosSaw.wav";
//    tmpOutName = "meshStretchTest.wav";
    tmpOutName = "test.wav";

    FileWvOut wavOut;
    QString* my_oFilename;
    my_oFilename = new QString(tmpOutDir + tmpOutName);
    std::string tmp = my_oFilename->toStdString();
    wavOut.openFile( tmp,
                     nO, FileWrite::FILE_WAV, Stk::STK_SINT16 );
    qDebug() << nO << "output channels";

    FileWvIn wavIn(1024,1024); // read incrementally so that normalization is ok
    QString* my_iFilename;
    my_iFilename = new QString(tmpInDir + tmpInName);
    tmp = my_iFilename->toStdString();
    wavIn.openFile( tmp ); // default format and correct normalization
    qDebug() << "strike files sample rate" << (double)wavIn.getFileRate();
    FileWvIn posIn(1024,1024); // read incrementally so that normalization is ok
    my_iFilename = new QString(tmpInDir + posInName);
    tmp = my_iFilename->toStdString();
    posIn.openFile( tmp ); // default format and correct normalization

    int i = 0;
    while ((!wavIn.isFinished()) && (!posIn.isFinished()))
//        for(int i = 0; i < 48000 * 16; i++)
    {
//        StkFrames floatOut( 1, 1 ); // fixed value test
//        floatOut[0] = 0.5;
//        wavOut.tick(wavIn.tick()); // straight wire test
        posIn.tick();
        StkFrames xy = posIn.lastFrame();
        xy[0] = -0.55;  xy[1] = 0.0; // static pos for article
        mesh->setInputPosition(xy[0], xy[1]);
//        qDebug() << xy[0] << xy[1];
StkFrames floatOut;
if (i < 160470)
floatOut = mesh->inputTick(wavIn.tick(),false);
        else
floatOut = mesh->inputTick(wavIn.tick(),true);
        wavOut.tick(floatOut);
i++;
    }
//    double tmp = *ibuffer++;
//    stk::StkFrames out = mesh->inputTick(tmp);
//    for ( k=0; k<nO; k++ )*obuffer++ = out[k];

}
