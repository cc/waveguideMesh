#ifndef GENMESH_H
#define GENMESH_H
#include <QVarLengthArray>
#include "stk/Instrmnt.h"
#include "stk/OnePole.h"
#include "stk/BiQuad.h"
#include "stk/PoleZero.h"
using namespace stk;

class GenMesh : public Instrmnt
{
public:
    GenMesh( unsigned short nX, unsigned short nY, unsigned short nO,
             StkFloat FS );
    ~GenMesh( void );
    //! Reset and clear all internal state.
    void clear( void );

    //! Set the x dimension size in samples.
    void setNX( unsigned short lenX );

    //! Set the y dimension size in samples.
    void setNY( unsigned short lenY );

    //    //! Set the z dimension size in samples.
    //    void setNZ( unsigned short lenZ );

    //! Set the x, y input position on a -1.0 - 1.0 scale.
    void setInputPosition( StkFloat xFactor, StkFloat yFactor );

    //! Set the stretch "factor" of the string (0.0 - 1.0).
    void setStretch( StkFloat stretch );

    //! Set the x, y output position on a -1.0 - 1.0 scale.
    void mapOutputPositions( StkFloat xFactor0, StkFloat yFactor0,
                             StkFloat xFactor1, StkFloat yFactor1 ,
                             bool adj = false);

    //    //! Set the loss filters gains (0.0 - 1.0).
    //    void setDecay( StkFloat decayFactor );

    //! Impulse the mesh with the given amplitude (frequency ignored).
    void noteOn( StkFloat frequency, StkFloat amplitude );

    //! Stop a note with the given amplitude (speed of decay) ... currently ignored.
    void noteOff( StkFloat amplitude );

    //! Input a sample to the mesh and compute one output sample.
    StkFrames& inputTick( StkFloat input , bool stretch);

    // unused Instrmnt class member functions
    void controlChange( int number, StkFloat value ){}

    StkFloat tick( unsigned int channel = 0 ){}

    StkFrames& tick( StkFrames& frames, unsigned int channel = 0 ){}
    unsigned short NX_, NY_, nOut;
    int **out;
//    StkFloat* iPhase;
//    StkFloat* phaseInc;
    StkFloat **xp;
    StkFloat **xm;
    StkFloat **yp;
    StkFloat **ym;
    StkFloat **xpz;
    StkFloat **xmz;
    StkFloat **ypz;
    StkFloat **ymz;
    StkFloat **vs;
    StkFloat** floatVect( int x, int y);
    OnePole  *filterX_;
    OnePole  *filterY_;
    BiQuad  **biquadX_;
    BiQuad  **biquadY_;
    // 2D interpolated input
    int xi0;
    int xi1;
    double xf0;
    double xf1;
    int yi0;
    int yi1;
    double yf0;
    double yf1;
    int counter_;
    QVarLengthArray<PoleZero*> dcBlocker;

};

#endif // GENMESH_H
