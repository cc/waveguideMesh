#ifndef STK_Mesh2Dcc_H
#define STK_Mesh2Dcc_H

#include "Instrmnt.h"
#include "OnePole.h"
#include "PoleZero.h"
#include "BiQuad.h"
#include "Delay.h"
#include "nyquistblock.h"
#include <QDebug>

namespace stk {

/***************************************************/
/*! \class Mesh2Dcc
    \brief Two-dimensional rectilinear waveguide mesh class.

    This class implements a rectilinear,
    two-dimensional digital waveguide mesh
    structure.  For details, see Van Duyne and
    Smith, "Physical Modeling with the 2-D Digital
    Waveguide Mesh", Proceedings of the 1993
    International Computer Music Conference.

    This is a digital waveguide model, making its
    use possibly subject to patents held by Stanford
    University, Yamaha, and others.

    Control Change Numbers:
       - X Dimension = 2
       - Y Dimension = 4
       - Mesh Decay = 11
       - X-Y Input Position = 1

    by Julius Smith, 2000 - 2002.
    Revised by Gary Scavone for STK, 2002.
*/
/***************************************************/

class DelayCC : public Delay
{
    int _x;
    int _y;
    QString _name;
public:
    void setSelf( int x, int y, QString name)
    {
        _x = x;
        _y = y;
        _name = name;
    }
#define NOQUOTE(x) x.toLatin1().data()
    StkFloat tick( StkFloat input )
    {
        StkFloat tmp = Delay::tick(input);
        qDebug() << NOQUOTE(_name) << _x << _y << NOQUOTE(QString("tick"))
                 << input << tmp;
        return tmp;
    }

    StkFloat tapOut( int )
    {
        StkFloat tmp = Delay::tapOut(0);
        qDebug() << NOQUOTE(_name) << _x << _y << NOQUOTE(QString("tap"))
                 << tmp;
        return tmp;
    }

    StkFloat lastOut( )
    {
        StkFloat tmp = Delay::lastOut();
        qDebug() << NOQUOTE(_name) << _x << _y << NOQUOTE(QString("last"))
                 << tmp;
        return tmp;
    }

};

class Mesh2Dcc : public Instrmnt
{
public:
    //! Class constructor, taking the x and y dimensions in samples.
    Mesh2Dcc( unsigned short nX, unsigned short nY, unsigned short nZ, unsigned short nO,
              StkFloat FS );

    //! Class destructor.
    ~Mesh2Dcc( void );

    //! Reset and clear all internal state.
    void clear( void );

    //! Set the x dimension size in samples.
    void setNX( unsigned short lenX );

    //! Set the y dimension size in samples.
    void setNY( unsigned short lenY );

    //! Set the z dimension size in samples.
    void setNZ( unsigned short lenZ );

    //! Set the x, y input position on a -1.0 - 1.0 scale.
    void setInputPosition( StkFloat xFactor, StkFloat yFactor );

    //! Set the stretch "factor" of the string (0.0 - 1.0).
    void setStretch( StkFloat stretch );

    //! Set the x, y output position on a -1.0 - 1.0 scale.
    void mapOutputPositions( StkFloat xFactor0, StkFloat yFactor0,
                             StkFloat xFactor1, StkFloat yFactor1 );

    //! Set the loss filters gains (0.0 - 1.0).
    void setDecay( StkFloat decayFactor );

    //! Impulse the mesh with the given amplitude (frequency ignored).
    void noteOn( StkFloat frequency, StkFloat amplitude );

    //! Stop a note with the given amplitude (speed of decay) ... currently ignored.
    void noteOff( StkFloat amplitude );

    //! Input a sample to the mesh and compute one output sample.
    StkFrames& inputTick( StkFloat input );

    // unused Instrmnt class member functions
    void controlChange( int number, StkFloat value ){}

    StkFloat tick( unsigned int channel = 0 ){}

    StkFrames& tick( StkFrames& frames, unsigned int channel = 0 ){}


    void tickVel(int i, StkFloat input);
protected:
    void clearMesh();
    StkFloat*** velocVect( int x, int y, int z );
    Delay** delayVect( int x, int y);
    DelayCC** delayVectCC( int x, int y, QString name);
    Delay**** velocDelay( int x, int y, int z, int a );

    unsigned short NX_, NY_, NZ_, nOut;
    OnePole  *filterX_;
    OnePole  *filterY_;
    PoleZero *dcBlock;
    StkFloat **v_;
    BiQuad  **biquadX_;
    BiQuad  **biquadY_;
    int **out;
    // select original or Delay UG version
    // #define GOOD 1
#ifdef GOOD
    StkFloat ***vxp; // [NX_][NY_][NZ_] positive-x velocity wave
    StkFloat ***vxm;
    StkFloat ***vyp;
    StkFloat ***vym;
#else
//    DelayCC **vxp; // [NX_][NY_][NZ_] positive-x velocity wave
//    DelayCC **vxm;
//    DelayCC **vyp;
//    DelayCC **vym;
    Delay **vxp; // [NX_][NY_][NZ_] positive-x velocity wave
    Delay **vxm;
    Delay **vyp;
    Delay **vym;
#endif

    int counter_; // time in samples
    // 2D interpolated input
    int xi0;
    int xi1;
    double xf0;
    double xf1;
    int yi0;
    int yi1;
    double yf0;
    double yf1;

    // non-interpolated stereo outputs
    int xo0;
    int xo1;
    int yo0;
    int yo1;

};

} // stk namespace

#endif
