#include "nyquistblock.h"
#include <math.h>
NyquistBlock::NyquistBlock()
{
    fConst0 = tanf((15707.963267948966f / 48000.0));
    fConst1 = (2 * (1 - (1.0f / (fConst0*fConst0))));
    fConst2 = (1.0f / fConst0);
    fConst3 = (1 + ((fConst2 - 1.4142135623730951f) / fConst0));
    fConst4 = (1.0f / (1 + ((1.4142135623730951f + fConst2) / fConst0)));
    for (int i=0; i<3; i++) fRec0[i] = 0;
}

float NyquistBlock::tick(float input)
{
    float output;
    fRec0[0] = (input - (fConst4 * ((fConst3 * fRec0[2]) + (fConst1 * fRec0[1]))));
    output = (fConst4 * (fRec0[2] + (fRec0[0] + (2 * fRec0[1]))));
    // post processing
    fRec0[2] = fRec0[1]; fRec0[1] = fRec0[0];
}

