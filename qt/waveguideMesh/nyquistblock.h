#ifndef NYQUISTBLOCK_H
#define NYQUISTBLOCK_H


class NyquistBlock
{
public:
    NyquistBlock();
    float tick(float input);
private:
  float 	fConst0;
  float 	fConst1;
  float 	fConst2;
  float 	fConst3;
  float 	fConst4;
  float 	fRec0[3];
};

#endif // NYQUISTBLOCK_H
