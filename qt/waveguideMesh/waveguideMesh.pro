#-------------------------------------------------
#
# Project created by QtCreator 2016-06-27T19:50:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = waveguideMesh
TEMPLATE = app

DEFINES += __UNIX_JACK__
DEFINES += __RT_AUDIO__
DEFINES += USE_SSE
QMAKE_CFLAGS +=  -O3 -mfpmath=sse -msse -msse2 -msse3 -ffast-math
SOURCES += main.cpp\
        mainwindow.cpp \
    nyquistblock.cpp \
    genmesh.cpp \
    fileio.cpp \
    wavfileio.cpp

HEADERS  += mainwindow.h \
    nyquistblock.h \
    genmesh.h \
    fileio.h \
    wavfileio.h

FORMS    += mainwindow.ui

LIBS += -lpthread -ljack

# stk latest from
# https://ccrma.stanford.edu/software/stk/download.html
# autoconf
# ./configure  --with-jack
# make, make install
# sudo ln /usr/local/lib/libstk-4.6.0.so /usr/lib64/
# includes preceded with stk/ should be ok as well as lib
LIBS += -lstk
# LIBS += -lrtaudio
