/***************************************************/
/*! \class Mesh2Dcc
    \brief Two-dimensional rectilinear waveguide mesh class.

    This class implements a rectilinear,
    two-dimensional digital waveguide mesh
    structure.  For details, see Van Duyne and
    Smith, "Physical Modeling with the 2-D Digital
    Waveguide Mesh", Proceedings of the 1993
    International Computer Music Conference.

    This is a digital waveguide model, making its
    use possibly subject to patents held by Stanford
    University, Yamaha, and others.

    Control Change Numbers:
       - X Dimension = 2
       - Y Dimension = 4
       - Mesh Decay = 11
       - X-Y Input Position = 1

    by Julius Smith, 2000 - 2002.
    Revised by Gary Scavone for STK, 2002.
*/
/***************************************************/

#include "Mesh2Dcc.h"
//#include "SKINImsg.h"
namespace stk {

Mesh2Dcc :: Mesh2Dcc(unsigned short nX, unsigned short nY, unsigned short nZ, unsigned short nO,
                     StkFloat FS) :
    nOut(nO)
{
#ifdef GOOD
    qDebug() <<"original";
#else
    qDebug() <<"delay line version";
#endif
    Stk::setSampleRate(FS);
    this->setNX( nX );
    this->setNY( nY );
    this->setNZ( nZ );

    filterX_ = new OnePole[nX];
    filterY_ = new OnePole[nY];
    dcBlock = new PoleZero[nO];
    biquadX_ = new BiQuad* [nX]; for(int i = 0; i < nX; i++) biquadX_[i] = new BiQuad[4]();
    biquadY_ = new BiQuad* [nY]; for(int i = 0; i < nY; i++) biquadY_[i] = new BiQuad[4]();

    // beware of non-init, use new StkFloat[n]() to init with zero
    v_ = new StkFloat* [nX-1]; for(int i = 0; i < nX-1; i++) v_[i] = new StkFloat[nY-1]();
    out = new int* [nO]; for(int i = 0; i < nO; i++) out[i] = new int[2]();

#ifdef GOOD
    vxp = velocVect( nX, nY, nZ);
    vxm = velocVect( nX, nY, nZ);
    vyp = velocVect( nX, nY, nZ);
    vym = velocVect( nX, nY, nZ);
#else
    //    vxp = delayVectCC( nX, nY, QString("xp"));
    //    vxm = delayVectCC( nX, nY, QString("xm"));
    //    vyp = delayVectCC( nX, nY, QString("yp"));
    //    vym = delayVectCC( nX, nY, QString("ym"));
    vxp = delayVect( nX, nY);
    vxm = delayVect( nX, nY);
    vyp = delayVect( nX, nY);
    vym = delayVect( nX, nY);
#endif

    StkFloat pole = 0.05; // 0.05
    unsigned short i;
    for ( i=0; i<nY; i++ ) {
        filterY_[i].setPole( pole );
        filterY_[i].setGain( 0.99 );
    }

    for ( i=0; i<nX; i++ ) {
        filterX_[i].setPole( pole );
        filterX_[i].setGain( 0.99 );
    }

    for ( i=0; i<nO; i++ ) {
        dcBlock[i].setBlockZero(0.99);
    }

    this->clear();
    lastFrame_.resize( 1, nO, 0.0 ); // resize for multi-channel out

}

Mesh2Dcc :: ~Mesh2Dcc( void )
{
}

void Mesh2Dcc :: clear( void )
{

    unsigned short i;
    for ( i=0; i<NY_; i++ )
        filterY_[i].clear();

    for ( i=0; i<NX_; i++ )
        filterX_[i].clear();

    for ( i=0; i<nOut; i++ )
        dcBlock[i].setBlockZero(0.99);

    counter_ = 0;
}

Delay ****Mesh2Dcc::velocDelay(int x, int y, int z , int a)
{
    Delay**** v;
    v = new Delay*** [x];
    for(int i = 0; i < x; i++)
    {
        v[i] = new Delay** [y];
        for(int j = 0; j < y; j++)
        {
            v[i][j] = new Delay*[z];
            for(int k = 0; k < z; k++)
                v[i][j][k] = new Delay[a];
        }
    }
    return v;
}

StkFloat*** Mesh2Dcc :: velocVect( int x, int y, int z )
{
    StkFloat*** v;
    v = new StkFloat** [x];
    for(int i = 0; i < x; i++)
    {
        v[i] = new StkFloat* [y]();
        for(int j = 0; j < y; j++)
            v[i][j] = new StkFloat[z]();
    }
    return v;
}


DelayCC** Mesh2Dcc :: delayVectCC( int x, int y, QString name)
{
    DelayCC** v;
    v = new DelayCC* [x];
    for(int i = 0; i < x; i++)
    {
        v[i] = new DelayCC [y];
        for(int j = 0; j < y; j++)
        {
            v[i][j].setDelay(1);
            v[i][j].setSelf(i,j,name);
        }
    }
    return v;
}

Delay** Mesh2Dcc :: delayVect( int x, int y)
{
    Delay** v;
    v = new Delay* [x];
    for(int i = 0; i < x; i++)
    {
        v[i] = new Delay [y];
        for(int j = 0; j < y; j++)
        {
            v[i][j].setDelay(1);
        }
    }
    return v;
}

void Mesh2Dcc :: setNX( unsigned short lenX )
{
    if ( lenX < 2 ) {
        oStream_ << "Mesh2Dcc::setNX(" << lenX << "): Minimum length is 2!";
        handleError( StkError::WARNING ); return;
    }

    NX_ = lenX;
}

void Mesh2Dcc :: setNY( unsigned short lenY )
{
    if ( lenY < 2 ) {
        oStream_ << "Mesh2Dcc::setNY(" << lenY << "): Minimum length is 2!";
        handleError( StkError::WARNING ); return;
    }

    NY_ = lenY;
}

void Mesh2Dcc :: setNZ( unsigned short lenZ )
{
    if ( lenZ < 2 ) {
        oStream_ << "Mesh2Dcc::setNZ(" << lenZ << "): Minimum length is 2!";
        handleError( StkError::WARNING ); return;
    }

    NZ_ = lenZ;
}

void Mesh2Dcc :: setDecay( StkFloat decayFactor )
{
    if ( decayFactor < 0.0 || decayFactor > 1.0 ) {
        oStream_ << "Mesh2Dcc::setDecay: decayFactor is out of range!";
        handleError( StkError::WARNING ); return;
    }

    int i;
    for ( i=0; i<NY_; i++ )
        filterY_[i].setGain( decayFactor );

    for (i=0; i<NX_; i++)
        filterX_[i].setGain( decayFactor );
}

// cc modified to make center the origin and do linear interp
void Mesh2Dcc :: setInputPosition( StkFloat xFactor, StkFloat yFactor )
{
    if ( xFactor < -1.0 || xFactor > 1.0 ) {
        oStream_ << "Mesh2Dcc::setInputPosition xFactor value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    if ( yFactor < -1.0 || yFactor > 1.0 ) {
        oStream_ << "Mesh2Dcc::setInputPosition yFactor value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    double xOrig = (NX_-1)*0.5;
    xFactor *= xOrig;
    xFactor += xOrig;
    xi0=(int)floor(xFactor);
    if(xi0<0)xi0=0;
    xi1=xi0+1;
    if(xi1>(NX_-1))xi1=(NX_-1);

    double xfrac = xFactor-(double)xi0;
    xf0= xfrac;
    xf1= 1.0-xfrac;

    double yOrig = (NY_-1)*0.5;
    yFactor *= yOrig;
    yFactor += yOrig;
    yi0=(int)floor(yFactor);
    if(yi0<0)yi0=0;
    yi1=yi0+1;
    if(yi1>(NY_-1))yi1=(NY_-1);

    double yfrac = yFactor-(double)yi0;
    yf0= yfrac;
    yf1= 1.0-yfrac;

}

void Mesh2Dcc :: mapOutputPositions( StkFloat xFactor0, StkFloat yFactor0,
                                     StkFloat xFactor1, StkFloat yFactor1)
{
    if ( xFactor0 < -1.0 || xFactor0 > 1.0 ) {
        oStream_ << "Mesh2Dcc::setOutputPosition xFactor0 value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    if ( yFactor0 < -1.0 || yFactor0 > 1.0 ) {
        oStream_ << "Mesh2Dcc::setOutputPosition yFactor0 value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    if ( xFactor1 < -1.0 || xFactor1 > 1.0 ) {
        oStream_ << "Mesh2Dcc::setOutputPosition xFactor1 value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    if ( yFactor1 < -1.0 || yFactor1 > 1.0 ) {
        oStream_ << "Mesh2Dcc::setOutputPosition yFactor value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    double xOrig = (NX_-1)*0.5;
    double yOrig = (NY_-1)*0.5;

    for(int i = 0; i < nOut; i++)
    {
        for(int j = 0; j < 2; j++)
        {
            double range = (j==0) ? (xFactor1 - xFactor0) : (yFactor1 - yFactor0);
            double frac = (double)i/(double)nOut;
            frac *= range;
            frac += (j==0) ? xFactor0 : yFactor0;
            frac *= (j==0) ? xOrig : yOrig;
            frac += (j==0) ? xOrig : yOrig;
            out[i][j]=1+(int)floor(frac);
        }
        qDebug() << "Mesh2Dcc mapOutputPositions:" << i << "x" << out[i][0] << "y" << out[i][1];
    }

}

void Mesh2Dcc :: noteOn( StkFloat frequency, StkFloat amplitude )
{
}

void Mesh2Dcc :: noteOff( StkFloat amplitude )
{
}

//#define outFilt(ch,x) (dcBlock[ch].tick(nyBlock[ch].tick(x))) // nyBlock not needed
//#define outFilt(ch,x) (0.5 * dcBlock[ch].tick(x)) // lower the gain
#define outFilt(ch,x) (3.0 * x) // output has DC
StkFrames& Mesh2Dcc::inputTick( StkFloat input )
{
    int i =counter_ % NZ_;
    //force
    //    vxp[xi0][yi0][i] += ((xf0+yf0)*input); // 2D
    //    vxp[xi1][yi0][i] += ((xf1+yf0)*input); // interpolation
    //    vxp[xi0][yi1][i] += ((xf0+yf1)*input);
    //    vxp[xi1][yi1][i] += ((xf1+yf1)*input);
    //    vyp[xi0][yi0][i] += ((xf0+yf0)*input);
    //    vyp[xi1][yi0][i] += ((xf1+yf0)*input);
    //    vyp[xi0][yi1][i] += ((xf0+yf1)*input);
    //    vyp[xi1][yi1][i] += ((xf1+yf1)*input);

    //        vxp[0][0][i] += (input);
    //        vyp[0][0][i] += (input);

    if(counter_)
        //                if(counter_ <1)
    {
        tickVel(i, input);

        for(int j = 0; j < nOut; j++)
        {
            int x = out[j][0];
            int y = out[j][1];
#ifdef GOOD
            //            lastFrame_[j] = vyp[x-1][y][i];
            lastFrame_[j] = vxp[x][y-1][i] + vyp[x-1][y][i];
#else
            lastFrame_[j] = vxp[x][y-1].lastOut() + vyp[x-1][y].lastOut();
#endif
            lastFrame_[j] = outFilt(j,lastFrame_[j]);
        }
    }
    counter_++;
    return lastFrame_;
}

const StkFloat VSCALE = 0.5;
//#define XIN(x,y,go) (((go==0)&&(x==0)&&(y==0)) ? 1.0 : 0.0)
//#define YIN(x,y,go) (((go==0)&&(x==0)&&(y==0)) ? 1.0 : 0.0)
#define XIN(x,y,in,go) (((go)&&(x==0)&&(y==0)) ? in : 0.0)
#define YIN(x,y,in,go) ((go)&&((x==0)&&(y==0)) ? in : 0.0)
#define DIN(go) ((go==0) ? 1.0 : 0.0)

void Mesh2Dcc::tickVel( int i, StkFloat input )
{
    //    input = DIN(i);
    int x, y;
    int j = (i+(NZ_-1)) % NZ_;
    // Update junction velocities.
    for (x=0; x<NX_-1; x++) {
        for (y=0; y<NY_-1; y++) {
            v_[x][y] = (
                        //                        (XIN(x,y,input,counter_)) + (YIN(x,y,input,counter_)) +
            #ifdef GOOD
                        vxp[x][y][i] + vxm[x+1][y][i] + vyp[x][y][i] +
                    vym[x][y+1][i] ) * VSCALE ;
#else
                        vxp[x][y].tapOut(0)
                        + vxm[x+1][y].tapOut(0)
                    +  vyp[x][y].tapOut(0)
                    + vym[x][y+1].tapOut(0)
                    ) * VSCALE ;
#endif
        }
    }
    v_[0][0] += 1.0*input;
    // Update junction outgoing waves, using alternate wave-variable buffers.
    for (x=0; x<NX_-1; x++) {
        for (y=0; y<NY_-1; y++) {
            StkFloat vxy = v_[x][y];

            // Update positive-going waves.
#ifdef GOOD
            vxp[x+1][y][j] = (vxy - vxm[x+1][y][i]);
            vyp[x][y+1][j] = (vxy - vym[x][y+1][i]);
#else
            vxp[x+1][y].tick( (vxy - vxm[x+1][y].tapOut(0)) );
#endif

            // Update minus-going waves.
#ifdef GOOD
            vxm[x][y][j] = (vxy - (vxp[x][y][i]));// + (XIN(x,y,input))));
            vym[x][y][j] = (vxy - (vyp[x][y][i])); //+ (YIN(x,y,input,counter_)));
#else
            vxm[x][y].tick( (vxy - (vxp[x][y].lastOut())));//+ XIN(x,y,counter_))));
            vyp[x][y+1].tick( (vxy - vym[x][y+1].tapOut(0)) );
            vym[x][y].tick( (vxy - (vyp[x][y].lastOut())));//+ YIN(x,y,counter_))));
#endif
        }
    }

    // Loop over velocity-junction boundary faces, update edge
    // reflections, with filtering.  We're only filtering on one x and y
    // edge here and even this could be made much sparser.
    for (y=0; y<NY_-1; y++) {
#ifdef GOOD
        vxp[0][y][j] = ( filterY_[y].tick(vxm[0][y][i]) );
#else
        vxp[0][y].tick( ( filterY_[y].tick(vxm[0][y].lastOut()) ) );
        vxp[0][y].tick( vxp[0][y].tapOut(0) );
#endif

#ifdef GOOD
        // Calculate allpass stretching.
        StkFloat temp = vxp[0][y][j];
        for (int i=0; i<4; i++)
            temp = biquadY_[y][i].tick(temp);
        vxp[0][y][j] = temp;
#else
        StkFloat temp = vxp[0][y].tapOut(0);
        for (int i=0; i<4; i++)
            temp = biquadY_[y][i].tick(temp);
        vxp[0][y].tick(temp);
        vxp[0][y].tick( vxp[0][y].tapOut(0));
#endif
#ifdef GOOD
        vxm[NX_-1][y][j] = ( vxp[NX_-1][y][i]); // + (XIN(NX_-1,y,input)));
#else
        vxm[NX_-1][y].tick(  ( vxp[NX_-1][y].lastOut())); // + (XIN(NX_-1,y,input,counter_))));
#endif
    }

    for (x=0; x<NX_-1; x++) {
#ifdef GOOD
        vyp[x][0][j] = ( filterX_[x].tick(vym[x][0][i]) );
#else
        vyp[x][0].tick( ( filterX_[x].tick(vym[x][0].lastOut()) ) );
        vyp[x][0].tick( vyp[x][0].tapOut(0) );
#endif

#ifdef GOOD
        // Calculate allpass stretching.
        StkFloat temp = vyp[x][0][j];
        for (int i=0; i<4; i++)
            temp = biquadX_[x][i].tick(temp);
        vyp[x][0][j] = temp;
#else
        StkFloat temp = vyp[x][0].tapOut(0);
        for (int i=0; i<4; i++)
            temp = biquadX_[x][i].tick(temp);
        vyp[x][0].tick(temp);
        vyp[x][0].tick( vyp[x][0].tapOut(0) );
#endif

#ifdef GOOD
        vym[x][NY_-1][j] = ( vyp[x][NY_-1][i] + (YIN(x,NY_-1,input,counter_)));
#else
        vym[x][NY_-1].tick(  ( vyp[x][NY_-1].lastOut() + (YIN(x,NY_-1,input,counter_))));
#endif
    }
}

void Mesh2Dcc :: setStretch( StkFloat stretch )
{
    int x, y;
    for (x=0; x<NX_; x++) {
        StkFloat lastFrequency_ = 500.0; // guess
        StkFloat coefficient;
        StkFloat freq = lastFrequency_ * 2.0;
        StkFloat dFreq = ( (0.5 * Stk::sampleRate()) - freq ) * 0.25;
        StkFloat temp = 0.5 + (stretch * 0.5);
        if ( temp > 0.9999 ) temp = 0.9999;

        for ( int i=0; i<4; i++ )	{
            coefficient = temp * temp;
            biquadX_[x][i].setA2( coefficient );
            biquadX_[x][i].setB0( coefficient );
            biquadX_[x][i].setB2( 1.0 );

            coefficient = -2.0 * temp * cos(TWO_PI * freq / Stk::sampleRate());
            biquadX_[x][i].setA1( coefficient );
            biquadX_[x][i].setB1( coefficient );

            freq += dFreq;
        }
    }
    for (y=0; y<NY_; y++) {
        StkFloat lastFrequency_ = 500.0; // guess
        StkFloat coefficient;
        StkFloat freq = lastFrequency_ * 2.0;
        StkFloat dFreq = ( (0.5 * Stk::sampleRate()) - freq ) * 0.25;
        StkFloat temp = 0.5 + (stretch * 0.5);
        if ( temp > 0.9999 ) temp = 0.9999;

        for ( int i=0; i<4; i++ )	{
            coefficient = temp * temp;
            biquadY_[y][i].setA2( coefficient );
            biquadY_[y][i].setB0( coefficient );
            biquadY_[y][i].setB2( 1.0 );

            coefficient = -2.0 * temp * cos(TWO_PI * freq / Stk::sampleRate());
            biquadY_[y][i].setA1( coefficient );
            biquadY_[y][i].setB1( coefficient );

            freq += dFreq;
        }
    }
}

} // stk namespace
