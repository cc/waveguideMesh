#include "genmesh.h"
#include <math.h>
#include <QDebug>
using namespace stk;

GenMesh :: GenMesh(unsigned short nX, unsigned short nY, unsigned short nO,
                   StkFloat FS) :
    nOut(nO)
{
    Stk::setSampleRate(FS);
    this->setNX( nX );
    this->setNY( nY );
    counter_ = 0;
    //    phaseInc = new double[nOut];
    //    iPhase = new double[nOut];
    //    for(int i = 0; i < nOut; i++) phaseInc[i] = (1000.0 + i*55.0)*2.0*3.14159265359 / FS;
    //    for(int i = 0; i < nOut; i++) iPhase[i] = 0.0;
    lastFrame_.resize( 1, nOut, 0.0 ); // resize for multi-channel out
    out = new int* [nO]; for(int i = 0; i < nO; i++) out[i] = new int[2]();
    xp = floatVect(nX,nY);
    xm = floatVect(nX,nY);
    yp = floatVect(nX,nY);
    ym = floatVect(nX,nY);
    xpz = floatVect(nX,nY);
    xmz = floatVect(nX,nY);
    ypz = floatVect(nX,nY);
    ymz = floatVect(nX,nY);
    vs = floatVect(nX,nY);
    filterX_ = new OnePole[nX];
    filterY_ = new OnePole[nY];
    biquadX_ = new BiQuad* [nX]; for(int i = 0; i < nX; i++) biquadX_[i] = new BiQuad[4]();
    biquadY_ = new BiQuad* [nY]; for(int i = 0; i < nY; i++) biquadY_[i] = new BiQuad[4]();
    this->clear();
    StkFloat pole = 0.04; // 0.1; // 0.05
    StkFloat gain = 0.99999999999999; // celletto  // 0.9999999 strikes // 0.9997 -- 1st damping // 59x42 exs 0.997
    for (int i=0; i<nY; i++ ) {
        filterY_[i].setPole( pole );
        filterY_[i].setGain( gain );
    }
    for (int i=0; i<nX; i++ ) {
        filterX_[i].setPole( pole );
        filterX_[i].setGain( gain );
    }
    dcBlocker.resize(nO);
    for (int i = 0; i < nO; i++) {
        dcBlocker[i] = new PoleZero();
        dcBlocker[i]->setBlockZero();
    }

}

void GenMesh :: clear( void )
{
    for (int y=0; y<NY_; y++ )
    {
        filterY_[y].clear();
        //        for(int i = 0; i < 4; i++) biquadY_[y][i].clear();
    }
    for (int x=0; x<NX_; x++ )
    {
        filterX_[x].clear();
        //        for(int i = 0; i < 4; i++) biquadX_[x][i].clear();
    }
}

// cc modified to make center the origin and do linear interp
void GenMesh :: setInputPosition( StkFloat xFactor, StkFloat yFactor )
{
    if ( xFactor < -1.0 || xFactor > 1.0 ) {
        oStream_ << "Mesh2Dcc::setInputPosition xFactor value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    if ( yFactor < -1.0 || yFactor > 1.0 ) {
        oStream_ << "Mesh2Dcc::setInputPosition yFactor value is out of range!";
        handleError( StkError::WARNING ); return;
    }

    double xOrig = (NX_-1)*0.5;
    xFactor *= xOrig;
    xFactor += xOrig;
    //    qDebug() << "xOrig" << xOrig << "xFactor" << xFactor;
    xi0=(int)floor(xFactor);
    if(xi0<0)xi0=0;
    xi1=xi0+1;
    if(xi1>(NX_-2))xi1=(NX_-2); // right side NX_-1 is outside update loop -1 is silent

    double xfrac = xFactor-(double)xi0;
    xf0= xfrac;
    xf1= 1.0-xfrac;

    double yOrig = (NY_-1)*0.5;
    yFactor *= yOrig;
    yFactor += yOrig;
    yi0=(int)floor(yFactor);
    if(yi0<0)yi0=0;
    yi1=yi0+1;
    if(yi1>(NY_-2))yi1=(NY_-2); // top side NY_-1 probably also outside update loop

    double yfrac = yFactor-(double)yi0;
    yf0= yfrac;
    yf1= 1.0-yfrac;

    //    // hardwired
    //    xi0 = 0;
    //    yi0 = 20;
    //    xi1 = 1;
    //    yi1 = 21;
    //qDebug() << "setInputPosition:" << "xi0" << xi0 << "yi0" << yi0
    //             << "xi1" << xi1 << "yi1" << yi1;

}

void GenMesh :: mapOutputPositions(StkFloat xFactor0, StkFloat yFactor0,
                                   StkFloat xFactor1, StkFloat yFactor1,
                                   bool adj)
{
    double xOrig = (NX_-1)*0.5;
    double yOrig = (NY_-1)*0.5;

    for(int i = 0; i < nOut; i++)
    {
        //        for(int j = 0; j < 2; j++)
        int j = 0; // x
        {
            double range = (j==0) ? (xFactor1 - xFactor0) : (yFactor1 - yFactor0);
            double frac = (double)(i+1)/(double)(nOut+1);
            frac *= range;
            frac += (j==0) ? xFactor0 : yFactor0;
            frac *= (j==0) ? xOrig : yOrig;
            frac += (j==0) ? xOrig : yOrig;
            out[i][j]=1+(int)floor(frac);
            if(adj) out[i][j]=(int)xOrig+i;
        }
        j = 1; // y
        {
            double range = (j==0) ? (xFactor1 - xFactor0) : (yFactor1 - yFactor0);
            double frac = (double)(i+1)/(double)(nOut+1);
            frac *= range;
            frac += (j==0) ? xFactor0 : yFactor0;
            frac *= (j==0) ? xOrig : yOrig;
            frac += (j==0) ? xOrig : yOrig;
            if(adj) frac = 0.0;
            out[i][j]=1+(int)floor(frac);
            if(adj) out[i][j]=(int)yOrig;
        }
        //        // hardwired
        //        out[0][0] = 29;
        //        out[0][1] = 21;
        //        out[1][0] = 34;
        //        out[1][1] = 21;
        qDebug() << i << "-- x" << out[i][0] << "y" << out[i][1];
    }

}

StkFloat** GenMesh :: floatVect( int x, int y)
{
    StkFloat** v;
    v = new StkFloat* [x];
    for(int i = 0; i < x; i++)
    {
        v[i] = new StkFloat [y]();
    }
    return v;
}

#define sum(x,y) vs[x][y] = 0.5 * (\
    xp [x]  [y] + \
    xm [x+1][y] + \
    yp [x]  [y] + \
    ym [x]  [y+1] \
    )
//    ); \
//    vs[x][y] = 0.9999 * vs[x][y] + 0.0001 * pow(vs[x][y], 3.0);
//vs[x][y] = 0.9999 * vs[x][y] + 0.0001 * pow(vs[x][y], 3.0);

#define xpTick(x,y) xpz [x+1] [y] = \
    xp [x+1] [y]
#define xpScat(x,y) xp [x+1] [y] = \
    vs[x][y] - xm [x+1] [y]

#define ypTick(x,y) ypz [x] [y+1] = \
    yp [x] [y+1]
#define ypScat(x,y) yp [x] [y+1] = \
    vs[x][y] - ym [x] [y+1]

#define xmTick(x,y) xmz [x] [y] = \
    xm [x] [y]
#define xmScat(x,y) xm [x] [y] = \
    vs[x][y] - xpz [x] [y]

#define ymTick(x,y) ymz [x] [y] = \
    ym [x] [y]
#define ymScat(x,y) ym [x] [y] = \
    vs[x][y] - ypz [x] [y]

StkFrames& GenMesh::inputTick( StkFloat input , bool stretch)
{

    //#define DIN(go) ((go==0) ? 1.0 : 0.0)
    //    input = DIN(counter_);
    //    if(counter_<6)
    {

        for (int x=0; x<NX_-1; x++) {
            for (int y=0; y<NY_-1; y++) {
                sum(x,y);
            }
        }
        //        vs[0][0]+=input;
        vs[xi0][yi0]+= xf0*yf0*input;
        vs[xi1][yi1]+= xf1*yf1*input;
        for (int x=0; x<NX_-1; x++) {
            for (int y=0; y<NY_-1; y++) {
                xpTick(x,y);
                xpScat(x,y);
                ypTick(x,y);
                ypScat(x,y);
                xmTick(x,y);
                xmScat(x,y);
                ymTick(x,y);
                ymScat(x,y);
            }
        }

#define xpEdgeTick(y) xpz [0] [y] = \
    xp [0] [y]
#define xpEdgeFilt(y) xp [0] [y] = \
    filterY_[y].tick(xmz [0] [y])

#define xmEdgeRefl(y) xm [NX_-1] [y] = \
    xpz [NX_-1] [y]
#define xmEdgeIn(y) xm [NX_-1] [y] = \
    input[y]
#define xmEdgeOut(y) output[y] = \
    xpz [NX_-1] [y]
#define stretchConst 0.5
#define sigDep 0.0
#define sigDep1(z) ((z<0.0) ? 0.02 : 0.002)
        for (int y=0; y<NY_-1; y++) {
            xpEdgeFilt(y);
            // Calculate allpass stretching.
setStretch(stretchConst + sigDep1(xp[0][y]));
// if (stretch)
    for (int i=0; i<4; i++)
                xp[0][y] = biquadY_[y][i].tick(xp[0][y]);
            xpEdgeTick(y);
            xmEdgeRefl(y);
            /* unused in final model
                        xmEdgeIn(y);
                        xmEdgeOut(y);
            */
        }

#define ypEdgeTick(x) ypz [x] [0] = \
    yp [x] [0]
#define ypEdgeFilt(x) yp [x] [0] = \
    filterX_[x].tick(ymz [x] [0])

#define ymEdgeRefl(x) ym [x] [NY_-1] = \
    ypz [x] [NY_-1]

#define ymEdgeIn(x) ym [x] [NY_-1] = \
    input[x]
#define ymEdgeOut(x) input[x] = \
    ypz [x] [NY_-1]

        for (int x=0; x<NX_-1; x++) {
            ypEdgeFilt(x);
            // Calculate allpass stretching.
            setStretch(stretchConst + sigDep1(yp[x][0]));
//            if (stretch)
                for (int i=0; i<4; i++)
                yp[x][0] = biquadX_[x][i].tick(yp[x][0]);
            ypEdgeTick(x);
            ymEdgeRefl(x);
            /* unused in final model
                        ymEdgeIn(x);
                        ymEdgeOut(x);
            */
        }

        for(int j = 0; j < nOut; j++)
        {
            int x = out[j][0];
            int y = out[j][1];
            lastFrame_[j] = xpz[x][y-1] + ypz[x-1][y];
            lastFrame_[j] = dcBlocker[j]->tick(lastFrame_[j]);
            lastFrame_[j] *= 5.0;
            //        lastFrame_[j] = outFilt(j,lastFrame_[j]); compare to 3x, no blocker
        }

        //            for(int i = 0; i < nOut; i++)
        //            {
        //                iPhase[i] += phaseInc[i];
        //                lastFrame_[i] = 0.1*sin(iPhase[i]);
        //            }
        counter_++;
    }
    return lastFrame_;
}

GenMesh :: ~GenMesh( void )
{
}

void GenMesh :: noteOn( StkFloat frequency, StkFloat amplitude )
{
}

void GenMesh :: noteOff( StkFloat amplitude )
{
}

void GenMesh :: setNX( unsigned short lenX )
{
    if ( lenX < 2 ) {
        oStream_ << "Mesh2Dcc::setNX(" << lenX << "): Minimum length is 2!";
        handleError( StkError::WARNING ); return;
    }

    NX_ = lenX;
}

void GenMesh :: setNY( unsigned short lenY )
{
    if ( lenY < 2 ) {
        oStream_ << "Mesh2Dcc::setNY(" << lenY << "): Minimum length is 2!";
        handleError( StkError::WARNING ); return;
    }

    NY_ = lenY;
}

//made to agree with architecture from reference
//https://ccrma.stanford.edu/~jos/pasp/Phasing_2nd_Order_Allpass_Filters.html
//void StifKarp :: setStretch( StkFloat stretch )
//{
//  stretching_ = stretch;
//  StkFloat a1;
//  StkFloat a2;
//  StkFloat freq = lastFrequency_ * 2.0;
//  StkFloat dFreq = ( (0.5 * Stk::sampleRate()) - freq ) * 0.25;
//  StkFloat R = 0.5 + (stretch * 0.5);
//  if ( R > 0.9999 ) R = 0.9999;
//  for ( int i=0; i<4; i++ )	{
//    a1 = -2.0 * R * cos(TWO_PI * freq / Stk::sampleRate());
//    a2 = R * R;
//    biquad_[i].setB0( a2 );
//    biquad_[i].setB1( a1 );
//    biquad_[i].setB2( 1.0 );
//    biquad_[i].setA1( a1 );
//    biquad_[i].setA2( a2 );

//    freq += dFreq;
//  }
//}

#define MAGIC 0.025  // was 0.25
void GenMesh :: setStretch( StkFloat stretch )
{
    int x, y;
    for (x=0; x<NX_; x++) {
        StkFloat lastFrequency_ = 500.0; // guess
        StkFloat coefficient;
        StkFloat freq = lastFrequency_ * 2.0;
        StkFloat dFreq = ( (0.5 * Stk::sampleRate()) - freq ) * MAGIC;
        StkFloat temp = 0.5 + (stretch * 0.5);
        if ( temp > 0.9999 ) temp = 0.999;

        for ( int i=0; i<4; i++ )	{
            coefficient = temp * temp;
            biquadX_[x][i].setA2( coefficient );
            biquadX_[x][i].setB0( coefficient );
            biquadX_[x][i].setB2( 1.0 );

            coefficient = -2.0 * temp * cos(TWO_PI * freq / Stk::sampleRate());
            biquadX_[x][i].setA1( coefficient );
            biquadX_[x][i].setB1( coefficient );

//            if(true )qDebug() << freq << dFreq << temp;
            freq += dFreq;
        }
    }
    for (y=0; y<NY_; y++) {
        StkFloat lastFrequency_ = 500.0; // guess
        StkFloat coefficient;
        StkFloat freq = lastFrequency_ * 2.0;
        StkFloat dFreq = ( (0.5 * Stk::sampleRate()) - freq ) * MAGIC;
        StkFloat temp = 0.5 + (stretch * 0.5);
        if ( temp > 0.9999 ) temp = 0.999;

        for ( int i=0; i<4; i++ )	{
            coefficient = temp * temp;
            biquadY_[y][i].setA2( coefficient );
            biquadY_[y][i].setB0( coefficient );
            biquadY_[y][i].setB2( 1.0 );

            coefficient = -2.0 * temp * cos(TWO_PI * freq / Stk::sampleRate());
            biquadY_[y][i].setA1( coefficient );
            biquadY_[y][i].setB1( coefficient );

            freq += dFreq;
        }
    }
}
