#ifndef WAVFILEIO_H
#define WAVFILEIO_H
#include "fileio.h"
#include "genmesh.h"

#include <QObject>

class WavFileIO : public FileIO
{
public:
    WavFileIO(int I, int O, int X, int Y);
    void start();
    void setMesh();
private:
    int nI;
    int nO;
    int nX;
    int nY;
    GenMesh *mesh;

};

#endif // WAVFILEIO_H
