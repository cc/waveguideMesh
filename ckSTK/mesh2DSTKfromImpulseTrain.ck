// chuck --srate:48000 mesh2DSTKfromImpulseTrain.ck

/*
getter method names have been aliased in 
Mesh2D.cpp in chugin
    QUERY->add_mfun(QUERY, mesh2d_setNX, "int", "x");
changes setNX() to x() and the same for all setters

///////////////// increase mesh size and recompile / install
const unsigned short NXMAX = 32;
const unsigned short NYMAX = 32;

/home/cc/Downloads/chugins-master/Mesh2D
make clean
make linux
sudo make install

///////////////// substitute inputTick for tick
#define INPUT_TICK
  
#ifdef INPUT_TICK
  //cc swap in inputTick for regular tick function
  //cc allows use of signal input for excitation instead of noteOn
  SAMPLE tick( SAMPLE in)
  {
    return m_mesh->inputTick(in);
  }
#else  
  // for Chugins extending UGen
  SAMPLE tick( SAMPLE in)
  {
    return m_mesh->tick(in);
  }
#endif

#ifdef INPUT_TICK
    //cc still needs to be called "tick" and need to set up for input
    QUERY->add_ugen_func(QUERY, mesh2d_tick, NULL, 1, 1);
#else
    // for UGen's only: add tick function
    QUERY->add_ugen_func(QUERY, mesh2d_tick, NULL, 0, 1);
#endif

*/

"../wav/" => string dir;

Impulse imp => LPF lpf1 => PoleZero pz => dac.chan(0);
lpf1.freq(3000.0);
imp => HPF hpf2 => pz;
hpf2.freq(12000.0);
pz.blockZero(0.95);

SndBuf exc => 
Mesh2D mesh => dac;
mesh.x(32);
mesh.y(32);
SndBuf2 snd;
dac => WvOut w => blackhole;
exc => blackhole;
exc.gain(10.0);
snd => blackhole;
w.wavFilename(dir + "mesh2DfromImpulseTrain.wav");
0.5::second => now;
exc.read(dir + "impulseTrain.wav");
snd.read(dir + "pos.wav");
true => int go;

now + 60::second => time quit;
while (now < quit)
{

snd.chan(0).last() => float xpos;
1.0 +=> xpos;
0.5 *=> xpos;
mesh.xpos(xpos);

snd.chan(1).last() => float ypos;
1.0 +=> ypos;
0.5 *=> ypos;
mesh.ypos(ypos);
  1::samp => now;
}
false => go;
5::second => now;
