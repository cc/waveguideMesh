// chuck -s --srate:48000 strikeMesh2Diter.ck
"../wav/" => string dir;

Impulse imp => LPF lpf1 => PoleZero pz => Mesh2D mesh => dac;
lpf1.freq(3000.0);
imp => HPF hpf2 => pz;
hpf2.freq(12000.0);
pz.blockZero(0.95);

mesh.x(32);
mesh.y(32);
dac => WvOut w => blackhole;
w.wavFilename(dir + "strikeMesh2Diter.wav");
0.05::second => now;
true => int go;

FileIO fin;
fin.open("../dat/iter.dat",FileIO.READ);
fun void clap()
{
0=> int ctr;
  while (go)
  {
if (ctr==20) {
  fin.close();
  fin.open("../dat/iter.dat",FileIO.READ);
  0=>ctr;
}
fin => float amp;
fin => float x;
fin => float y;
fin => float sec;
fin => float l;
fin => float h;
fin.readLine();
ctr++;
lpf1.freq(3000.0 + 2950.0*l);
hpf2.freq(12000.0 + 1100.0*h);
    imp.next(amp);
mesh.xpos(x);
mesh.ypos(y);
    (.1 - 0.01*sec)::second => now;
  }
}
spork ~clap();

now + 60::second => time quit;
while (now < quit)
{
  1::samp => now;
}
false => go;
5::second => now;
