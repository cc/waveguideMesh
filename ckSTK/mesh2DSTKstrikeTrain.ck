// chuck -s --srate:48000 mesh2DSTKstrikeTrain.ck

/*

const unsigned short NXMAX = 32;
const unsigned short NYMAX = 32;

/home/cc/Downloads/chugins-master/Mesh2D
make clean
make linux
sudo make install

getter method names have been aliased in 
Mesh2D.cpp
    QUERY->add_mfun(QUERY, mesh2d_setNX, "int", "x");
changes setNX() to x() and the same for all setters
*/


"../wav/" => string dir;

Impulse imp => blackhole;

Mesh2D mesh => dac;
mesh.x(32);
mesh.y(32);
SndBuf2 snd;
dac => WvOut w => blackhole;
snd => blackhole;
w.wavFilename(dir + "mesh2DstrikeTrain.wav");
imp => WvOut w2 => blackhole;
w2.wavFilename(dir + "impulseTrain.wav");
0.5::second => now;
snd.read(dir + "pos.wav");
true => int go;

fun void clap()
{
  while (go)
  {

Std.rand2f(0.0,1.0) => float amp;
    imp.next(amp);

snd.chan(0).last() => float xpos;
1.0 +=> xpos;
0.5 *=> xpos;
mesh.xpos(xpos);

snd.chan(1).last() => float ypos;
1.0 +=> ypos;
0.5 *=> ypos;
mesh.ypos(ypos);

mesh.noteOn(amp);
    .1::second => now;
  }
}
spork ~clap();

now + 60::second => time quit;
while (now < quit)
{
  1::samp => now;
}
false => go;
5::second => now;
