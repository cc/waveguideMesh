// chuck -s --srate:48000 mesh2D32x8inputTick.ck
// logmap fires fake strikes into mesh inputTick

"../wav/" => string dir;

Impulse imp => LPF lpf1 => PoleZero pz => Mesh2D mesh => dac;
lpf1.freq(3000.0);
imp => HPF hpf2 => pz;
hpf2.freq(12000.0);
hpf2.gain(-1.0);
pz.blockZero(0.95);
Step unity;
unity => Envelope r => blackhole;
unity => Envelope x => blackhole;
unity => Envelope y => blackhole;
mesh.x(32);
mesh.y(8);
// mesh.decay(0.99999);
WvOut2 w => blackhole;
w.wavFilename(dir + "iterPos.wav");
Step minus;
minus.gain(-1.0);
x => Gain xx => w.chan(0);
xx.gain(2.0);
minus => w.chan(0);
y => Gain yy => w.chan(1);
yy.gain(2.0);
minus => w.chan(1);
pz => WvOut w1 => blackhole;
w1.wavFilename(dir + "iterTrain.wav");
mesh => WvOut w2 => blackhole;
w2.wavFilename(dir + "mesh2D32x8inputTick.wav");
0.05::second => now;
true => int go;

r.value(3.7);
r.target(3.7);
r.duration(10.0::second);
1::samp => now;
fun float F(float y)  { return (r.last()*y*(1.0 - y)); };
0.0 => float z1 => float z2 => float z3 => float z4;    
0.20001 => float y0;
for(0=>int i; i<100; i++) { map(); }

float y1;
fun void map()
{
  F(y0)  => y1;
  z3 => z4;
  z2 => z3;
  z1 => z2;
  y0 => z1;
  y1 => y0;
}
fun void clap()
{
  while (go)
  {
    map();
    imp.next(z2);
    x.target(y1);
    map();
    y.target(y1);
    z2 => float sec;
    y1 => float l;
    z4 => float h;
2.0 *=> l;
1.0 -=> l;
2.0 *=> h;
1.0 -=> h;
//    lpf1.freq(3000.0 + 2950.0*l);
//    hpf2.freq(12000.0 + 1100.0*h);
<<<2000.0 + 1970.0*l>>>;
    lpf1.freq(2000.0 + 1970.0*l);
    hpf2.freq(12000.0 + 1100.0*h);
    (.01 + 0.2*sec)::second => dur d;
    d => now;
    x.duration(d);
    y.duration(d);
  }
}
spork ~clap();

now + 10::second => time quit;
while (now < quit)
{
  mesh.xpos(x.last());
  mesh.ypos(y.last());
  1::samp => now;
}
false => go;
5::second => now;
